<?php

/**
 * MAGENTO - RUSH ORDERS POST SYSTEM
 * GREEN GLOBAL Co.,ltd
 *
 * @author bangvn
 * @link bangvn@toancauxanh.vn
 *
 * @uses this file is used when a cron run.
 */
function repostcronXML() {
    umask(0777);
    Mage::log("Reposting Process Started", null, "reposting-service" . ".log");
    //Mage::log(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB));
    //Mage::log(getcwd());

    $serverdir = "/home/admin/domains/himalayanbowls.com/public_html/";
    $dir = $serverdir . "post/";
    $xml_files = array();
// Open a known directory, and proceed to read its contents
    try {
        if ($dh = opendir($dir)) {
            Mage::log(" Opening this directory", null, "reposting-service" . ".log");
            while (($file = readdir($dh)) !== false) {
                Mage::log(" Reading directory", null, "reposting-service" . ".log");
                if (empty($file) || $file === "." || $file === "..") {
                    Mage::log("Reading empty entry ", null, "reposting-service" . ".log");
                } else {
                    Mage::log($file . "File found", null, "reposting-service" . ".log");
                    $xml_files[] = $file;
                }
            }
            closedir($dh);
        } else {
            Mage::log(" Can not open this directory", null, "reposting-service" . ".log");
        }
    } catch (Exception $ex) {
        Mage::log($ex, null, "reposting-service" . ".log");
    }



    if (empty($xml_files)) {
        Mage::log("There is no repost file found", null, "reposting-service" . ".log");
    } else {
        Mage::log("Order going to repost :", null, "reposting-service" . ".log");
        Mage::log($xml_files, null, "reposting-service" . ".log");



        // Processing /post folder, retreiving filenames
        foreach ($xml_files as $xml_file) {

            // Read the file name
            $xml_file_name = explode('.', $xml_file);

            // Read order ID from file name
            $order_id = $xml_file_name[0];

            // Information file name , located in /postinfo folder
            $info_file = $serverdir . 'postinfo' . '/' . $order_id . '.info';

            // Load the infomation
            $info_string = file_get_contents($info_file);

            // Load retrying count
            $info_retrying_count = intval($info_string);

            // Load XML data
            $xml_file = $serverdir . 'post' . '/' . $xml_file;

            $xml_string = file_get_contents($xml_file);

            // Convert XML data to associated array
            $xml_assoc = (array) simplexml_load_string($xml_string, 'SimpleXMLElement', LIBXML_NOCDATA);

            if ($info_retrying_count == 6) {

                // This order has failed to post 6 times
                $response_message = "This order has failed to post 6 times - Order ID #" . $order_id;
                $response_body = 'Order #' . $order_id . ' This order has failed to post 6 times';
                Mage::log($response_message, null, "reposting-service" . ".log");
                Mage::log($response_body, null, "reposting-service" . ".log");

                Mage::log($order_id, null, "reposting-failed" . ".log");
                $status_code = 'failed';
                unlink($xml_file);
                sendErrorEmail($response_message, $response_body, 'failed', $order_id, $xml_string);

                Mage::log("Reposting #" . $order_id . " end. This order currently out of 6 times reposting", null, "reposting-service" . ".log");
            } else {

                // Repost this order a again
                $info_retrying_count++;

                Mage::log("Reposting order #" . $order_id . " started", null, "reposting-service" . ".log");
                $reposted = sendXMLFile($xml_string, $order_id, $info_retrying_count);

                if ($reposted === true) {

                    Mage::log("Reposting order #" . $order_id . " Success", null, "reposting-service" . ".log");
                } else {

                    Mage::log("Reposting order #" . $order_id . " failed", null, "reposting-service" . ".log");
                }
                Mage::log("Reposting order #" . $order_id . " ended", null, "reposting-service" . ".log");
            }
        }
    }

    unset($xml_files);
    unset($dir);


    Mage::log("Reposting Process ended", null, "reposting-service" . ".log");
    return true;
}

function successAction() {

}

function failAction() {

}

/*
 * This function
 */

function sendXMLFile($arr, $order_ID, $info_retrying_count = 1) {
    $serverdir = "/home/admin/domains/himalayanbowls.com/public_html/";
    $xml = $arr;
    /*
     * $xml = '<?xml version="1.0"?>' . PHP_EOL . $xml;
     */

    $target_url = "https://orders.rushorder.com/xml3/parser.cfm";

    $url = Mage::getStoreConfig('pro/checkout/url');
    if (!empty($url)) {
        $target_url = $url;
    }

    //$target_url = "http://api.localhost.com/get.php";
    $target_url = "https://api.rushorder.com/orders/index.cfm";


    $response_message = "";
    $response_body = "";
    $status_code = 'posting';
    $retrying_count = 0;

    while ($status_code == 'posting' || $status_code == 'retry') {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $target_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// Prepare the xml file
        if ($status_code == 'retry') {
            if ($retrying_count == 3) {
                // Quit after retrying 3 times
                $response_message = "Rush Order Reposting Process Failed - Order ID #" . $order_ID;
                $response_body = 'Order #' . $order_ID . ' repost failed due to no response from Rush Order after 3 retries, please try again later';
                Mage::log($response_message);
                Mage::log($response_body);

                Mage::log($order_ID, null, "reposting-service-failed" . ".log");
                $status_code = 'failed';
                sendErrorEmail($response_message, $response_body, 'Success', $order_ID, $xml);

                file_put_contents($serverdir . 'post' . '/' . $order_ID . '.xml', $xml);
                file_put_contents($serverdir . 'postinfo' . '/' . $order_ID . '.info', $info_retrying_count);

                break;
            } else {
                Mage::log($xml, null, $order_ID . "-repost" . ".log");
                Mage::log('Retrying reposting Order to ' . $target_url . ' : ' . $retrying_count . 'times. Order ID #' . $order_ID, null, "reposting-service-details" . ".log");
            }
        } else {
            Mage::log($xml, null, $order_ID . "-repost" . ".log");
            Mage::log('Reposting Order to ' . $target_url, null, "reposting-service-details" . ".log");
        }
// Repost the xml
        $result = curl_exec($ch);
        curl_close($ch);
// Loging the responsed
        Mage::log($result, null, $order_ID . "-repost-result" . ".log");
        Mage::log('Result from ' . $target_url);
        if (empty($result) || !isset($result)) {
            //Retry reposting
            Mage::log('Empty result, retrying reposting', null, "reposting-service-details" . ".log");
            $response_message = "";
            $status_code = 'retry';
            $retrying_count++;
            continue;
        }

        $response = (array) simplexml_load_string($result, 'SimpleXMLElement', LIBXML_NOCDATA);
        Mage::log('Response from ' . $target_url);
        Mage::log($response, null, $order_ID . "-repost-response" . ".log");
        Mage::log($response);

        if (empty($response['order']) || !isset($response['order'])) {
            //Retry reposting
            Mage::log('Can not analyse response, retrying reposting', null, "reposting-service-details" . ".log");
            $response_message = "";
            $status_code = 'retry';
            $retrying_count++;
            continue;
        }
        $attrs = $response['order']->attributes();
        Mage::log($attrs, null, "reposting-service-details" . ".log");

        foreach ($attrs as $key => $attr) {
            $attr_val = (array) $attr[0];
            if ($key == 'status') {
                $status_code = $attr_val[0];
            }
        }
        Mage::log($status_code, null, "reposting-service-details" . ".log");
        $filename = $serverdir . 'post' . '/' . $order_ID . '.xml';

        if ($status_code == 'Success') {
            //Stop reposting , email administrator

            unlink($filename);

            $response_message = "Rush Order Reposting Success - Order ID #" . $order_ID;
            $response_body = 'Order #' . $order_ID . ' reposted';

            Mage::log($response_message, null, "reposting-service-details" . ".log");
            Mage::log($response_body, null, "reposting-service-details" . ".log");

            sendErrorEmail($response_message, $response_body, 'Success', $order_ID, $xml);
            break;
        } else {
            if ($status_code == 'Duplicate') {
                //Stop reposting, email administrator

                unlink($filename);

                $response_message = "Duplicated Repost, Skipping ...!";
                $response_body = 'Order #' . $order_ID . ' Duplicated <br>';
                $response_description = $response;

                Mage::log($response_message, null, "reposting-service-details" . ".log");
                Mage::log($response_body, null, "reposting-service-details" . ".log");
                Mage::log($response_description, null, "reposting-service-details" . ".log");

                sendErrorEmail($response_message, $response_body, 'Success', $order_ID, $xml);
                break;
            } else {
                if ($status_code == 'failed') {
                    //Stop reposting, email administrator

                    unlink($filename);

                    $response_message = "Rush Order Reposting Process Failed! - Order ID #" . $order_ID;
                    $response_body = 'Order #' . $order_ID . ' was failed, please contact administrator';
                    $response_error = '';
                    $response_description = '';
                    $order_childs = $response['order']->children();
                    foreach ($order_childs as $key => $order_child) {
                        $childs = (array) $order_child[0];
                        if ($key == 'error') {
                            $response_error = $childs[0];
                        } else {
                            if ($key == 'text') {
                                $response_description = $childs[0];
                            }
                        }
                    }
                    Mage::log($response_message, null, "reposting-service-details" . ".log");
                    Mage::log($response_body, null, "reposting-service-details" . ".log");
                    Mage::log('Error code : ' . $response_error, null, "reposting-service-failed" . ".log");
                    Mage::log('Error descriptions : ' . $response_description, null, "reposting-service-failed" . ".log");

                    Mage::log($order_ID, null, "reposting-service-failed" . ".log");

                    $response_body .= '<br>' . 'Error code : ' . $response_error . '<br>' . 'Error Description : ' . $response_description . '<br>';
                    sendErrorEmail($response_message, $response_body, 'Success', $order_ID, $xml);
                    break;
                } else {
                    //Retry reposting
                    $response_message = "";
                    $status_code = 'retry';
                    $retrying_count++;
                }
            }
        }
    }


    Mage::log('OVERVIEW for order #' . $order_ID, null, "reposting-service-details" . ".log");
    Mage::log($response_message, null, "reposting-service-details" . ".log");
    Mage::log($response_body, null, "reposting-service-details" . ".log");
    Mage::log('Reposting Process for order #' . $order_ID . ' end, overal status : ' . $status_code . '. Connection is now close.', null, "reposting-service-details" . ".log");


    return true;
}

function sendErrorEmail($subject, $body, $mail_code = 'Success', $order_id = null, $xml_body = null) {
    $fromEmail = "support@himalayanbowls.com"; // sender email address
    $fromName = "Himalayanbowls Administrator"; // sender name

    $toEmail1 = "joseph@himalayanbowls.com"; // recipient email address
    $toName1 = "Joseph"; // recipient name

    $toEmail2 = "bangvn@webdev.vn"; // recipient email address
    $toName2 = "Bang Vu Ngoc"; // recipient name

    $toEmail3 = "pahlin@rushorder.com"; // recipient email address
    $toName3 = "Patrik Ahlin"; // recipient name

    $toEmail4 = "baotv@toancauxanh.vn"; // recipient email address
    $toName4 = "Tran Vu Bao"; // recipient name

    $mail = new Zend_Mail();
    $mail->setBodyText($body);
    $mail->setFrom($fromEmail, $fromName);

    if ($mail_code == 'Success') {
        //$mail->addTo($toEmail2, $toName2);
    } else {
        $mail->addTo($toEmail1, $toName1);
        //$mail->addTo($toEmail2, $toName2);
        $mail->addTo($toEmail3, $toName3);
        $mail->addTo($toEmail4, $toName4);
    }
    if (isset($order_id) && isset($xml_body)) {
        $at = $mail->createAttachment($xml_body);
        $at->disposition = Zend_Mime::DISPOSITION_INLINE;
        $at->encoding = Zend_Mime::ENCODING_BASE64;
        $at->filename = $order_id . '.xml';
    }


    $mail->setSubject($subject);
    try {
        $mail->send();
    } catch (Exception $ex) {
        // I assume you have your custom module.
        // If not, you may keep 'customer' instead of 'yourmodule'.
        Mage::getSingleton('core/session')->addError(Mage::helper('pro')->__('Unable to send email.'));
        Mage::log($ex);
        return false;
    }

    return true;
}

?>