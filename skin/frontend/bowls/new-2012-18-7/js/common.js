//Cufon replace
//Cufon.replace('h1, h2, h3, h4, .him-top-menu ul li, .regular-price, .button, .button-big, .note', {
//    hover: true, 
//    fontFamily: 'Proxima Nova Rg'
//});
//Cufon.replace('.button1, .button1-big', {
//    textShadow: '1px 1px 0 #a43815', 
//    fontFamily: 'Proxima Nova Rg'
//});
//Cufon.replace('.him-announce', {
//    fontFamily: 'Proxima Nova Rg'
//});

//No conflict with prototype
jQuery.noConflict();

//Video play
jQuery(document).ready(function() {
    /* pretty photo responds on rel='prettyPhoto' */
    jQuery("a[rel^='prettyPhoto']").prettyPhoto();
});

//Prepare the toggle
//jQuery(document).ready(function(){
//    jQuery('#toggler').toggle(
//        function(){ // you can add as much here as you'd like
//            jQuery('#tgbody').show('slow');
//            jQuery('#imgArrows').attr('src','../images/tg-open.jpg');
//        }, function() { // same here
//            jQuery('#tgbody').hide('slow');
//            jQuery('#imgArrows').attr('src','../images/tg-closed.jpg');
//        });
//});
jQuery(document).ready(function(){
    //Hide (Collapse) the toggle containers on load
    //$(".toggle_container").hide();
    //Switch the "Open" and "Close" state per click then slide up/down (depending on open/close state)
    jQuery("h3.trigger, #faq h3").click(function(){
        jQuery(this).toggleClass("active").next().slideToggle("slow");
    });
});
