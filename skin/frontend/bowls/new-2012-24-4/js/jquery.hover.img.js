jQuery(document).ready(function($){
	//Caption Sliding (Partially Hidden to Visible)
	$('.boxgrid-01.caption').hover(function(){
		$(".cover", this).stop().animate({top:'206px'},{queue:false,duration:160});
	}, function() {
		$(".cover", this).stop().animate({top:'245px'},{queue:false,duration:160});
	});
	$('.boxgrid-02.caption').hover(function(){
		$(".cover", this).stop().animate({top:'186px'},{queue:false,duration:160});
	}, function() {
		$(".cover", this).stop().animate({top:'245px'},{queue:false,duration:160});
	});
	$('.boxgrid-03.caption').hover(function(){
		$(".cover", this).stop().animate({top:'165px'},{queue:false,duration:160});
	}, function() {
		$(".cover", this).stop().animate({top:'245px'},{queue:false,duration:160});
	});
	$('.boxgrid-04.caption').hover(function(){
		$(".cover", this).stop().animate({top:'145px'},{queue:false,duration:160});
	}, function() {
		$(".cover", this).stop().animate({top:'245px'},{queue:false,duration:160});
	});
	$('.boxgrid-05.caption').hover(function(){
		$(".cover", this).stop().animate({top:'124px'},{queue:false,duration:160});
	}, function() {
		$(".cover", this).stop().animate({top:'245px'},{queue:false,duration:160});
	});
});