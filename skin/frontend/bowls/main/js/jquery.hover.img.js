jQuery(document).ready(function(){
	//Caption Sliding (Partially Hidden to Visible)
	jQuery('.boxgrid-01.caption').hover(function(){
		jQuery(".cover", this).stop().animate({top:'190px'},{queue:false,duration:160});
	}, function() {
		jQuery(".cover", this).stop().animate({top:'221px'},{queue:false,duration:160});
	});
	jQuery('.boxgrid-02.caption').hover(function(){
		jQuery(".cover", this).stop().animate({top:'168px'},{queue:false,duration:160});
	}, function() {
		jQuery(".cover", this).stop().animate({top:'221px'},{queue:false,duration:160});
	});
	jQuery('.boxgrid-03.caption').hover(function(){
		jQuery(".cover", this).stop().animate({top:'146px'},{queue:false,duration:160});
	}, function() {
		jQuery(".cover", this).stop().animate({top:'221px'},{queue:false,duration:160});
	});
	jQuery('.boxgrid-04.caption').hover(function(){
		jQuery(".cover", this).stop().animate({top:'124px'},{queue:false,duration:160});
	}, function() {
		jQuery(".cover", this).stop().animate({top:'221px'},{queue:false,duration:160});
	});
	jQuery('.boxgrid-05.caption').hover(function(){
		jQuery(".cover", this).stop().animate({top:'105px'},{queue:false,duration:160});
	}, function() {
		jQuery(".cover", this).stop().animate({top:'221px'},{queue:false,duration:160});
	});
});