<?php

/**
 * MAGENTO - RUSH ORDERS PING SYSTEM
 * GREEN GLOBAL Co.,ltd
 * 
 * @author bangvn
 * @link bangvn@toancauxanh.vn
 * 
 * @uses this file is used when a cron run.
 */
?>
<?php

/*
 * Do the ping process via cronjob
 * 
 * @using ping_system.log
 */

function doPingProcess() {

        $msg = 'Ping System Started on ' . date('Ymd-His') . ' ! ';
        Mage::log($msg, null, 'ping_system.log');

        //doShipment('100000457', '123456');
        //die;
        //
        // LIST PROCESSING ORDERS
        $ping_orders = listProcessingOrder();

        print_r('get.php doPingProcess $ping_orders');
        print_r("<pre>");
        print_r($ping_orders);
        print_r("<pre>");

        // NO PROCESSING ORDERS ON MAGENTO
        if (empty($ping_orders) || !is_array($ping_orders)) {

                $msg = 'No order is processing 1, exit';
                Mage::log($msg, null, 'ping_system.log');
                return;
        } else {

                // SPLIT ORDERS INTO 24 EACH STEP
                $_ordersArr = array();
                $_countOrder = 0;
                $_countTurn = 0;
                foreach ($ping_orders as $ping_order) {
                        $_ordersArr[$_countTurn][] = $ping_order;
                        $_countOrder++;
                        if ($_countOrder % 24 == 0) {
                                $_countTurn++;
                        }
                }
        }

        if (empty($_ordersArr) || !is_array($_ordersArr)) {

                $msg = 'No order is processing 2, exit';
                Mage::log($msg, null, 'ping_system.log');
                return;
        } else {
                // PROCESSING PING LIST ORDERS STATUS WITH 24 ORDERS PER STEPS
                $_step = 0;
                foreach ($_ordersArr as $_orderArr) {
                        $ping_orders_string = implode(",", $_orderArr);
                        $ping_list = pingRushOrder($ping_orders_string, $_step);
                        $_step++;
                }
        }

        $msg = 'Ping System End on ' . date('Ymd-His') . ' ! ';
        Mage::log($msg, null, 'ping_system.log');
}
?>

<?php

/*
 * Ping get the detail for 24 processing orders per request
 * 
 * @val $ping_orders_string
 * @val $step
 * @using ping_system_repsponse.log
 * @using ping_system_exception.log
 * @using ping_system.log
 * @return boolean success
 * 
 */

function pingRushOrder($ping_orders_string, $step) {

        // BUILD THE TARGET URL TO PING 
        $target_url = "http://tools.rushorder.com/wizmo2/status.cfm?code=HYC&altorder=" . $ping_orders_string;

        print_r("<pre>");
        print_r($target_url);
        print_r("<pre>");

        $msg = 'Starting ping the list from : ' . $target_url;
        Mage::log($msg, null, 'ping_list_system.log');

        // REQUEST OVERVIEW STATUS FOR CANDIDATE ORDERS
        try {

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $target_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
                $result = curl_exec($ch);
                curl_close($ch);
        } catch (Exception $e) {

                Mage::log($e, null, 'ping_list_system_exception.log');
                return false;
        }
        
        // CHECK THE RESULT FOR EMPTY
        if (empty($result) || !isset($result)) {

                // RETURN FALSE FOR THIS LIST
                $msg = 'The result is unknow due to connection problem';
                Mage::log($msg, null, 'ping_list_system.log');
                return false;
        } else {

                // LOG THE RESULT
                Mage::log($result, null, 'ping_list_system_repsponse.log');
        }

        // LOAD THE RESULT TO RESPONSE ARRAY
        $response = simplexml_load_string($result, 'SimpleXMLElement');
        
        // CHECK THE LOADED XML RESPONSE
        if (empty($response)) {

                $msg = 'The response in unknow due to unrecognized format of result!';
                Mage::log($msg, null, 'ping_list_system.log');
                return false;
        } else {

                // ANALYSE THE RESPONSE ARRAY TO ASSOC ARRAY
                Mage::log($response, null, 'ping_list_system_repsponse.log');
                $orders_response = analyseListResponse($response);
        }
        /*
         *  @orders_response = array(@alt_order, @internetid, @number, @order_date, @status)
         */

        if (empty($orders_response) || !is_array($orders_response)) {

                $msg = 'The analysed response is unknow due to unrecognized format of response data!';
                Mage::log($msg, null, 'ping_list_system.log');
                return false;
        } else {

                // SELECT ORDERS ID THAT ALREADY SHIPPED THEN PING FOR DETAIL ON RUSH ORDER
                $ping_detail_orders = alreadyShipped($orders_response);
        }

        // CHECK IF NO ORDER ON THIS LIST NEED TO UPDATE STATUS
        if (empty($ping_detail_orders) || !is_array($ping_detail_orders)) {

                $msg = 'No order on this list need to create shipment!';
                Mage::log($msg, null, 'ping_list_system.log');
                return false;
        } else {

                // PING ARRAY OF ORDERS DETAILS
                $msg = 'Pinging details for this list started!';
                Mage::log($msg, null, 'ping_list_system.log');
                
                $msg = 'For Orders listing below : ';
                Mage::log($ping_detail_orders, null, 'ping_list_system.log');
                $ping_details = pingOrderDetails($ping_detail_orders);
                
                $msg = 'Pinging details for this list ended!';
                Mage::log($msg, null, 'ping_list_system.log');
        }

        // OVERALL RESULT FOR STEP
        if ($ping_details) {

                // PING DETAILS ALL SUCCESS
                $msg = 'Step ' . $step . ' ping success! ' . date('Ymd-His');
                Mage::log($msg, null, 'ping_list_system.log');
        } else {

                $msg = 'Step ' . $step . ' contain some error(s), please contact your administrator';
                Mage::log($msg, null, 'ping_list_system.log');
                return false;
        }

        return true;
}

/*
 * Find all already shipped orders
 * 
 * @return array ids of shipped orders retreived from rush order
 */

function alreadyShipped($orders_response) {
        $ping_detail_orders = array();
        foreach ($orders_response as $order_detail) {
                $candidate_status = $order_detail['status'];
                $pos_shipped_on = strpos($candidate_status, 'Shipped on');
                $pos_ready_to_ship = strpos($candidate_status, 'Ready to Ship');

                if ($pos_shipped_on === false) {
                        // NOT SHIPPED YET ORDERS
                        if ($pos_ready_to_ship === false) {
                                
                        } else {
                                
                        }
                } else {
                        // ALREADY SHIPPED ORDERS
                        $ping_detail_orders[] = $order_detail['alt_order'];
                }
        }
        return $ping_detail_orders;
}

/*
 * Analyse the response Simple Element Object
 * @response Simple Element Object responsed
 * @return array
 */

function analyseListResponse($response) {
        $orders_response = array();
        foreach ($response as $order) {
                $attrs = $order->attributes();
                $order_response = array();
                foreach ($attrs as $key => $attr) {
                        $attr_val = (array) $attr[0];
                        if ($key == 'alt_order') {
                                $order_response['alt_order'] = $attr_val[0];
                        }
                        if ($key == 'internetid') {
                                $order_response['internetid'] = $attr_val[0];
                        }
                        if ($key == 'number') {
                                $order_response['number'] = $attr_val[0];
                        }
                        if ($key == 'order_date') {
                                $order_response['order_date'] = $attr_val[0];
                        }
                }
                $value = (array) $order;
                $order_response['status'] = $value[0];
                $orders_response[] = $order_response;
        }
        return $orders_response;
}

/*
 * Create shippment & add tracking number to specified orders
 * @orderId system ID of order
 * @trackNumber tracking number for usps shipping service
 */

function doShipment($orderId, $trackNumber, $title = "USPS Priority Mail") {

        if (!isset($trackNumber) || empty($trackNumber) || ($trackNumber == "")) {

                $msg = 'Can not do shippment with empty tracking number - Order ID : #' . $orderId;
                Mage::log($msg, null, 'ping_detail_system_exceptions.log');

                $mailSubject    = "Adding Details for shippment on Himalayanbowls failed!";
                $mailBody       = $msg;
                $code           = "dev";
                sendNotifyEmail($mailSubject, $mailBody, $code);

                return false;
        }

        //$carrier = "usps";
        $carrier = "custom";
        //$title = "United States Postal Service";
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);

        if ($order->canShip()) {

                $itemQty = $order->getItemsCollection()->count();
                $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment($itemQty);
                $shipment = new Mage_Sales_Model_Order_Shipment_Api();

                try {
                        
                        $shipmentId = $shipment->create($orderId, array(), null, false);
                        $shipment->addTrack($shipmentId, $carrier, $title, $trackNumber);

                        $shipment = Mage::getModel('sales/order_shipment')->loadByIncrementId($shipmentId);
                        $shipment->sendEmail(true)->setEmailSent(true);

                        $msg = 'Shipment #' . $shipmentId . ' for orders #' . $orderId . ' created, tracking number #' . $trackNumber . ' of "USPS" was created !';
                        Mage::log($msg, null, 'ping_detail_system.log');
				        print_r("<pre>");
				        print_r($msg);
				        print_r("<pre>");

                        $mailSubject    = "Adding Details for shippment on Himalayanbowls ok!";
                        $mailBody       = $msg;
                        $code           = "dev";
                        sendNotifyEmail($mailSubject, $mailBody, $code);

                } catch (Exception $e) {

                        Mage::log($e, null, 'ping_detail_system_exceptions.log');

                        $mailSubject    = "Adding Details for shippment on Himalayanbowls failed!";
                        $mailBody       = $e;
                        $code           = "dev";
                        sendNotifyEmail($mailSubject, $mailBody, $code);

                        return false;
                }
        } else {

                $msg = 'Order ID : #' . $orderId . '. This order can not ship yet.';
                Mage::log($msg, null, 'ping_detail_system_exceptions.log');

                $mailSubject    = "Adding Details for shippment on Himalayanbowls failed!";
                $mailBody       = "Order #' . $orderId . ' have no tracking number on Rush Order, please contact your administrator.";
                $code           = "dev";
                sendNotifyEmail($mailSubject, $mailBody, $code);

                print_r("<pre>");
		        print_r($msg);
		        print_r("<pre>");
                return false;
        }
        return true;
}

/*
 * List all processing Order inside Magento
 * @return array of ids
 */

function listProcessingOrder() {

        //2011-11-04 0:0:0
        $destination_date = '2014-01-01 00:00:00';
        //$destination_date = date("y-m-d H:i:s",strtotime("-6 months"));
        // $destination_date = date("y-m-d H:i:s",strtotime("-1 months"));
        $today = date('Y-m-d H:i:s');

        // FIND ALL PROCESSING ORDER INSIDE MAGENTO 
        $orders = Mage::getResourceModel('sales/order_collection')
                ->addAttributeToSelect('*')
                ->addFieldToFilter('status', array("in" => array('processing')))
                ->addAttributeToFilter('store_id', Mage::app()->getStore()->getId())
                ->addAttributeToFilter('created_at', array("from" => $destination_date, "to" => $today, "datetime" => true))
                ->addAttributeToSort('created_at', 'asc')
                ->load();

        $ping_orders = array();
        $ping_orders_string = '';

        foreach ($orders as $order):
                $ping_orders[] = $order->getIncrementId();
        endforeach;

        return $ping_orders;
}

function sendNotifyEmail($subject, $body, $mail_code = 'success') {
        //SENDER
        $fromEmail = "support@himalayanbowls.com";
        $fromName = "Himalayanbowls Administrator";

        //RECIPIENT 1
        $toEmail1 = "joseph@himalayanbowls.com";
        $toName1 = "Joseph";

        //RECIPIENT 2
        $toEmail2 = "bangvn@greenglobal.vn";
        $toName2 = "Bang Vu Ngoc";

        $mail = new Zend_Mail();
        $mail->setSubject($subject);
        $mail->setBodyText($body);

        $mail->setFrom($fromEmail, $fromName);

        if ($mail_code == 'success') {
                //$mail->addTo($toEmail2, $toName2);
        } else {
                if ($mail_code == 'dev') {
                        $mail->addTo($toEmail2, $toName2);
                } else {
                        $mail->addTo($toEmail1, $toName1);
                        //$mail->addTo($toEmail2, $toName2);
                }
        }

        try {
                $mail->send();

                $msg = 'Notify mail sent! ';
                Mage::log($msg, null, 'ping_detail_system.log');
        } catch (Exception $ex) {
                Mage::log($ex);

                $msg = 'Notify mail not sent! ';
                Mage::log($msg, null, 'ping_detail_system.log');

                return false;
        }
        return true;
}

function pingOrderDetails($ping_detail_orders) {

        
        print_r("<pre>");
        print_r($ping_detail_orders);
        print_r("<pre>");

        foreach ($ping_detail_orders as $order_detail_id) {
                print_r("================================================================================================ <br>");
                //$target_url = "http://tools.rushorder.com/wizmo2/index.cfm?code=HYC&altorder=" . $order_detail_id['alt_order'];
                $target_url = "http://tools.rushorder.com/wizmo2/index.cfm?code=HYC&altorder=" . $order_detail_id;

                print_r("<pre>");
                print_r($target_url);
                print_r("<pre>");

                $msg = 'Pinging : ' . $target_url;
                Mage::log($msg, null, 'ping_details_system_response.log');

                // REQUEST OVERVIEW STATUS FOR CANDIDATE ORDERS
                try {
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $target_url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
                        $result = curl_exec($ch);
                        curl_close($ch);
                } catch (Exception $e) {
                        Mage::log($e, null, 'ping_detail_system_exceptions.log');
                        return false;
                }

                // CATCH THE RESULT
                $response = simplexml_load_string($result, 'SimpleXMLElement');

                if (empty($response)) {
                        $msg = 'Ping details process order #' . $order_detail_id . ' failed, response can not be recognized!';
                        Mage::log($msg, null, 'ping_detail_system.log');
                        return false;
                }

                Mage::log($response, null, 'ping_details_system_response.log');

                $orderID            = $response->altorder;
                $shipping_details   = $response->shippingdetails;

                $shipping_details_arr = (array)$shipping_details;

                $isNormal = true;
                foreach ($shipping_details_arr['shipment'] as $key => $shipment) {
                    $shipping_details_arr_shipment_attr = $shipment->attributes();
                    if (isset($shipping_details_arr_shipment_attr['CODE'])) {
                        print_r("This is multiple shipment <br>");
                        $isNormal = false;
                        break;
                    }else{
                        print_r("This is normal shipment <br>");
                        break;
                    }
                }

                if ($isNormal) {
                    // Normal shipment
                    $shipping_shipment  = $shipping_details->shipment;
                    $trackNumber        = $shipping_shipment->trackingnumber;
                    $title = $shipping_shipment->description;

                    if (empty($trackNumber) || !isset($trackNumber) || ($trackNumber == "")) {

                        $mailSubject    = "Adding Details for shippment on Himalayanbowls failed!";
                        $mailBody       = "Order #' . $order_detail_id . ' have no tracking number on Rush Order, please contact your administrator.";
                        $code           = "dev";
                        sendNotifyEmail($mailSubject, $mailBody, $code);

                        $msg = 'Ping details process fail for order #' . $order_detail_id . ' !, check order ID & tracking number';
                        Mage::log($msg, null, 'ping_detail_system.log');

                        print_r($msg);
                        print_r("<br/>");
                        continue;
                    }

                    if (isset($orderID)) {
                        // CREATE THE SHIPMENT AND ADD TRACKING NUMBER

                        $msg = 'Orders #' . $orderID . ' created, tracking number #' . $trackNumber . ' of "USPS" was created !';
                        Mage::log($msg, null, 'ping_detail_system.log');
                        
                        // Temporary disable doShipment
                        doShipment($orderID, $trackNumber, $title);

                        print_r($msg);
                        print_r("<br/>");

                    } else {
                        $msg = 'Ping details process order #' . $order_detail_id . ' fail!, check order ID & tracking number';
                        Mage::log($msg, null, 'ping_detail_system.log');

                        $mailSubject    = "Adding Details for shippment on Himalayanbowls failed!";
                        $mailBody       = "Order #' . $order_detail_id . ' have no tracking number on Rush Order, please contact your administrator.";
                        $code           = "dev";
                        sendNotifyEmail($mailSubject, $mailBody, $code);

                        print_r($msg);
                        print_r("<br/>");
                        continue;
                    }
                }else{
                    // Multiple shipment
                    print_r("Multiple shipment processing <br>");

                    foreach ($shipping_details_arr['shipment'] as $key => $shipment) {
                        $shipping_details_arr_shipment_attr = $shipment->attributes();
                        if (isset($shipping_details_arr_shipment_attr['CODE'])) {
                            $trackNumber  = $shipment->trackingnumber;
                            $title = $shipment->description;

                            if (empty($trackNumber) || !isset($trackNumber) || ($trackNumber == "")) {
                                $mailSubject    = "Adding Details for shippment on Himalayanbowls failed!";
                                $mailBody       = "Order #' . $order_detail_id . ' have no tracking number on Rush Order, please contact your administrator.";
                                $code           = "dev";
                                sendNotifyEmail($mailSubject, $mailBody, $code);

                                $msg = 'Ping details process fail for order #' . $order_detail_id . ' !, check order ID & tracking number';
                                Mage::log($msg, null, 'ping_detail_system.log');

                                print_r($msg);
                                print_r("<br/>");
                                continue;
                            }
                            if (isset($orderID)) {
                                // CREATE THE SHIPMENT AND ADD TRACKING NUMBER

                                $msg = 'Orders #' . $orderID . ' created, tracking number #' . $trackNumber . ' of "USPS" was created !';
                                Mage::log($msg, null, 'ping_detail_system.log');
                                
                                // Temporary disable doShipment
                                doShipment($orderID, $trackNumber,$title);

                                print_r($msg);
                                print_r("<br/>");

                            } else {
                                $msg = 'Ping details process order #' . $order_detail_id . ' fail!, check order ID & tracking number';
                                Mage::log($msg, null, 'ping_detail_system.log');

                                $mailSubject    = "Adding Details for shippment on Himalayanbowls failed!";
                                $mailBody       = "Order #' . $order_detail_id . ' have no tracking number on Rush Order, please contact your administrator.";
                                $code           = "dev";
                                sendNotifyEmail($mailSubject, $mailBody, $code);

                                print_r($msg);
                                print_r("<br/>");
                                continue;
                            }

                        }
                    }
                    

                }

                continue;
                
        }
        return true;
}
?>