��    ]           �      �     �     �  "        )     D     Q     b     h     o     x     �     �     �  1   �  1   �  0   �  /   ,	     \	     p	     ~	     �	     �	     �	  ,   �	     �	     �	     
     /
     6
     ;
     A
     Q
     Y
     h
  
   m
     x
     �
  8   �
  /   �
               #     4     E  "   \          �     �     �  
   �     �     �     �  	   �     �     �       	     K        ^     m     ~     �     �     �     �     �     �  !        $     ;     I  
   U     `     f     m     q     y     ~     �     �     �     �     �     �  %   �     �     �  	   �     �     �     �  �  �     �     �  &   �  *        9     F     [     a     g     p  	   x     �     �  :   �  6   �  9     5   <     r     �     �     �     �     �  +   �       -   '     U     k     s          �  	   �     �     �     �     �     �  G      =   H     �     �     �     �     �  %   �          "     )     <  	   H     R  	   _     i     �     �     �     �     �  V   �               $  $   2     W     ^     o     �     �  ,   �  "   �       	             %     +     2  	   5     ?     E     J     O     T     \     d     h  ,   m     �     �  	   �  	   �     �     �         T   7              ]          )             L      
                  V   -              +   B   <       ,   M       C       #   E   	       '   (   \   ?       >      =       /   X       !   K              :            D   N   4   @          J                   3   [   P   W   ;          G             9   5   R          Q   8                 O   .         F   &      U       "   2           *      1   $      6       I      Z      S      Y   A       %   0   H           %d  comments , a subalbum of - - - Comments box activated - - - - - - Rating enabled - - - --- none --- --- separate --- 1 day 1 hour 1 minute 1 month 1 second 1 week 1 year <b>ERROR: Attempt to enter an invalid rating.</b> <b>ERROR: Illegal attempt to enter a comment.</b> <b>ERROR: Illegal attempt to enter a rating.</b> <b>ERROR: Illegal attempt to upload a file.</b> Average&nbsp;rating Browse photos Click to start/stop Comment added Comment edited Could not process comment Double click to start/stop slideshow running Edit! Enter/modify photo description Error during upload Faster Home Image Leave a comment Link to My&nbsp;rating Next Next photo Next&nbsp;page No album defined yet. No albums or photos found matching your search criteria. Only gif, jpg and png image files are supported Page is not available. Photo Photo Of The Day Photo not found. Please enter a comment Please enter a valid email address Please enter your name Prev. Prev.&nbsp;page Previous photo Rating: %s Searchstring: Send! Sidebar Slideshow Slideshow Slower Start Stop There are To see the full size images, you need to enable javascript in your browser. Top Ten Photos Top rated photos Upload Photo Uploaded file is not an image View View the album View the cover photo View the cover photos View the top rated photos You must login to enter a comment You must login to vote Your comment: Your email: Your name: album albums and average days high hours low minutes months of photo photo albums. The last album added is photos seconds very high very low weeks years Project-Id-Version: wp-photo-album-plus
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-09-18 17:44+0100
PO-Revision-Date: 2011-09-18 17:48+0100
Last-Translator: J.N. Breetvelt <OpaJaap@OpaJaap.nl>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __a
X-Poedit-Basepath: ..
X-Poedit-Language: Dutch
X-Poedit-Country: NETHERLANDS
X-Poedit-SearchPath-0: theme
X-Poedit-SearchPath-1: .
 %d reacties , een sub-album van - - - Reactiesysteem geactiveerd - - - - - - Waarderingssysteem geactiveerd - - - --- geen --- --- afzonderlijk --- 1 dag 1 uur 1 minuut 1 maand 1 seconde 1 week 1 jaar <b>FOUT: Poging een ongeldige waardering in te voeren.</b> <b>FOUT: Verboden poging een reactie in te voeren.</b> <b>FOUT: Verboden poging een waardering in te voeren.</b> <b>FOUT: Verboden poging een bestand te uploaden.</b> Gem.&nbsp;waardering Foto's bladeren Klik om te starten/stoppen Reactie toegevoegd Reactie bewerkt Kan de reactie niet verwerken Dubbel klik voor start/stop diavoorstelling Bewerk! Voer in of wijzig de beschrijving van de foto Fout bij het uploaden Sneller Beginpagina Foto Geef een reactie Link naar Mijn&nbsp;waardering Volgende Volgende foto Volgende&nbsp;pagina Er is nog geen album ingesteld. Er zijn geen albums of foto's gevonden die voldoen aan uw zoekopdracht. Allen gif, jpg an png afbeeldingsbestanden worden ondersteund Pagina is niet beschikbaar. Foto Foto van de dag Foto niet gevonden. Voer a.u.b een reactie in Voer a.u.b. een geldig email adres in Voer a.u.b. uw naam in Vorige Vorige&nbsp;pagina Vorige foto %s punten Zoeken naar: Verstuur! Zijkolom Diavoorstelling Diavoorstelling Trager Start Stop Er zijn Om foto's op volledige grootte te zien, dient u javascript te activeren in uw browser. Top Tien foto's Top waarderingen Foto Uploaden Ge-upload bestand is geen afbeelding Bekijk Bekijk het album Bekijk de omslagfoto Bekijk de omslagfoto's Bekijk de top score foto's U moet ingelogd zijn om een reactie te geven U moet ingelogd zijn om te stemmen Uw reactie: Uw email: Uw naam: album albums en gemiddeld dagen hoog uren laag minuten maanden van foto foto albums. Het laatst toegevoegde album is foto's seconden zeer hoog zeer laag weken jaren 