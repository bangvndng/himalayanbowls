��    /      �  C                   )     D     Q  1   b     �     �     �     �     �     �     �     �     �  8        G     ^     d     u     �  
   �     �     �  	   �     �     �     �  	   �     �     �     �          !     7     N     T     [     _     g     l     p     s  %   y     �  	   �     �  <  �  ,   �     #	     <	     K	  E   b	     �	     �	     �	  	   �	     �	     �	     
     
  -   5
  K   c
  $   �
     �
     �
  !   �
       
   #     .  $   <     a     w     ~     �     �     �     �     �     �     �  !   �          *  	   7     A     H     O     V     X  E   `     �     �     �                  .               	      +      *                    /   !   (   
   -                                             ,                              &          %              #                $   '          "         )           , a subalbum of - - - Rating enabled - - - --- none --- --- separate --- <b>ERROR: Attempt to enter an invalid rating.</b> Average&nbsp;rating Browse photos Faster Home Leave a comment Link to My&nbsp;rating Next&nbsp;page No album defined yet. No albums or photos found matching your search criteria. Page is not available. Photo Photo Of The Day Photo not found. Prev.&nbsp;page Rating: %s Searchstring: Sidebar Slideshow Slideshow Slower Start Stop There are Top rated photos View View the album View the cover photo View the cover photos You must login to vote album albums and average high low of photo photo albums. The last album added is photos very high very low Project-Id-Version: WP Photo Album Plus 2.4.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-09-18 17:44+0100
PO-Revision-Date: 2011-09-18 17:44+0100
Last-Translator: J.N. Breetvelt <OpaJaap@OpaJaap.nl>
Language-Team: BNG NET <webmaster@bng.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Japanese
X-Poedit-Country: JAPAN
X-Poedit-SourceCharset: UTF-8
X-Poedit-Bookmarks: -1,157,-1,-1,-1,-1,-1,-1,-1,-1
X-Poedit-KeywordsList: __a
X-Poedit-Basepath: ..
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: theme
 、次のアルバムのサブアルバム:  - - - 評価有効 - - - --- なし --- --- セパレート--- <b>エラー: 不当な評価の入力が試みられました。</b> 平均&nbsp;評価 写真をブラウズ 速く ホーム コメントを残す リンク先: わたしの&nbsp;評価 次の&nbsp;ページ アルバムが定義されていません。 検索条件に見合ったアルバムや写真は見あたりません。 ページが利用できません。 写真 本日の写真 写真が見当たりません。 前の&nbsp;ページ 投票: %s 検索文字: サイドバースライドショー スライドショー 遅く 開始 停止   評価の高い写真 閲覧 アルバムの閲覧 カバーフォトの閲覧 カバーフォトの閲覧  投票にはログインが必須 アルバム  アルバム および 平均 高い 低い / 写真  件のフォトアルバムがあります。最新のアルバムは 写真 非常に高い 非常に低い 