��    P      �  k         �     �     �     �                    %     ,     5     =     F     M  1   T     �     �     �  ,   �     �     �     �     �               "  
   '     2     A  8   W     �     �     �     �     �  "   �     		      	     &	     6	  
   E	     P	     ^	     d	  	   v	     �	     �	     �	  	   �	  K   �	     �	     �	     
     
     
     1
     G
  !   a
     �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
  %        *     1  	   9     C     L     R  �  X          (  #   ?     c     r     �     �     �     �     �     �     �  F   �     4     J  '   b  H   �     �     �     �     �            
   *     5     K  &   `  f   �     �  
             $     @  2   W     �  
   �     �     �     �     �               (     >     N     W     `  k   i  &   �  /   �  
   ,     7     K     h  6   �  ;   �  )   �     $     7     V  
   ^     i     x  
   {     �     �     �     �     �     �     �  
   �  F   �       
   "     -     =     M     Z     >           /   9         
   ,   *   E      F      1   A       B           8       0   2   3          +       7       O   K   P       D   ?      N   5          $      C          )   @      J       %      (   M          H          L           6               :       	      '             <          &       #       .              ;   !            -      4      "             =      I   G          %d  comments , a subalbum of - - - Rating enabled - - - --- none --- --- separate --- 1 day 1 hour 1 minute 1 month 1 second 1 week 1 year <b>ERROR: Attempt to enter an invalid rating.</b> Average&nbsp;rating Browse photos Click to start/stop Double click to start/stop slideshow running Edit! Faster Home Leave a comment Link to My&nbsp;rating Next Next photo Next&nbsp;page No album defined yet. No albums or photos found matching your search criteria. Page is not available. Photo Photo Of The Day Photo not found. Please enter a comment Please enter a valid email address Please enter your name Prev. Prev.&nbsp;page Previous photo Rating: %s Searchstring: Send! Sidebar Slideshow Slideshow Slower Start Stop There are To see the full size images, you need to enable javascript in your browser. Top Ten Photos Top rated photos View View the album View the cover photo View the cover photos View the top rated photos You must login to enter a comment You must login to vote Your comment: Your email: Your name: album albums and average days high hours low minutes months of photo photo albums. The last album added is photos seconds very high very low weeks years Project-Id-Version: wppa
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-09-18 17:44+0100
PO-Revision-Date: 2011-09-18 17:44+0100
Last-Translator: J.N. Breetvelt <OpaJaap@OpaJaap.nl>
Language-Team: OpaJaap <OpaJaap@OpaJaap.nl>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __a
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: utf-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: theme
 הערות %d , תת אלבום של - - - דירוג מאופשר - - - --- ריק --- --- נפרד --- יום אחד שעה אחת דקה אחת חודש אחד שנייה אחת שבוע אחד שנה אחת <b>שגיאה: ניסיון להכניס דירוג לא תקין.</b> דירוג ממוצע דפדף בתמונות הקלק כדי להתחיל/לעצור הקלק קליק כפול להתחיל/לעצור מצגת תמונות עריכה! מהר יותר בית השאר הערה קישור ל הדירוג שלי קדימה התמונה הבאה עמוד&nbsp;הבא לא הוגדר עדיין אלבום. לא נמצאו אלבומים או תמונות התואמים את שאילתת החיפוש שלך. עמוד לא זמין תמונה תמונת היום תמונה לא נמצאה. אנא הזן הערה אנא הכנס כתובת אימייל תקינה אנא הכנס שמך אחורה עמוד&nbsp;קודם תמונה קודמת דירוגים: %s מחרוזת חיפוש: שליחה! מצגת סרגל צד מצגת תמונות לאט יותר התחל הפסק ישנם כדי לראות את התמונות בגודל מלא עליך לאפשר javascript בדפדפן שלה. עשר התמונות המובילות התמונות שמדורגות הכי גבוה צפייה צפה באלבום צפה בתמונת השער צפה בתמונות השער צפה בתמונות שמדורגות הכי גבוה חייבים להתחבר על מנת להכניס הערה חובה להתחבר כדי להצביע ההערה שלך: כתובת הדוא"ל שלך: שמך: אלבום אלבומים ו ממוצע ימים גבוה שעות נמוך דקות חודשים מ תמונה אלבומי תמונות. האלבום האחרון שנוסף הוא תמונות שניות גבוה מאד נמוך מאד שבועות שנים 