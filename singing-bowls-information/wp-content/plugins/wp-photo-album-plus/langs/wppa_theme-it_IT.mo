��    ]           �      �     �     �  "        )     D     Q     b     h     o     x     �     �     �  1   �  1   �  0   �  /   ,	     \	     p	     ~	     �	     �	     �	  ,   �	     �	     �	     
     /
     6
     ;
     A
     Q
     Y
     h
  
   m
     x
     �
  8   �
  /   �
               #     4     E  "   \          �     �     �  
   �     �     �     �  	   �     �     �       	     K        ^     m     ~     �     �     �     �     �     �  !        $     ;     I  
   U     `     f     m     q     y     ~     �     �     �     �     �     �  %   �     �     �  	   �     �     �     �  5  �     /     <  #   Q     u     �     �     �     �     �     �  	   �     �     �  5   �  9   $  5   ^  5   �     �     �     �     �       "   :  2   ]  	   �  #   �  1   �     �     �     �               !     /     5     E     W  9   n  5   �     �     �     �            #   3     W     m     s     �     �     �     �     �  	   �     �     �     �     �  M   �     E     R     f  $   r     �  
   �     �     �     �      �          +     <     J     W     ]     c     e     n     u     z     ~     �     �     �     �  )   �     �     �  
   �     �  	   �     �         T   7              ]          )             L      
                  V   -              +   B   <       ,   M       C       #   E   	       '   (   \   ?       >      =       /   X       !   K              :            D   N   4   @          J                   3   [   P   W   ;          G             9   5   R          Q   8                 O   .         F   &      U       "   2           *      1   $      6       I      Z      S      Y   A       %   0   H           %d  comments , a subalbum of - - - Comments box activated - - - - - - Rating enabled - - - --- none --- --- separate --- 1 day 1 hour 1 minute 1 month 1 second 1 week 1 year <b>ERROR: Attempt to enter an invalid rating.</b> <b>ERROR: Illegal attempt to enter a comment.</b> <b>ERROR: Illegal attempt to enter a rating.</b> <b>ERROR: Illegal attempt to upload a file.</b> Average&nbsp;rating Browse photos Click to start/stop Comment added Comment edited Could not process comment Double click to start/stop slideshow running Edit! Enter/modify photo description Error during upload Faster Home Image Leave a comment Link to My&nbsp;rating Next Next photo Next&nbsp;page No album defined yet. No albums or photos found matching your search criteria. Only gif, jpg and png image files are supported Page is not available. Photo Photo Of The Day Photo not found. Please enter a comment Please enter a valid email address Please enter your name Prev. Prev.&nbsp;page Previous photo Rating: %s Searchstring: Send! Sidebar Slideshow Slideshow Slower Start Stop There are To see the full size images, you need to enable javascript in your browser. Top Ten Photos Top rated photos Upload Photo Uploaded file is not an image View View the album View the cover photo View the cover photos View the top rated photos You must login to enter a comment You must login to vote Your comment: Your email: Your name: album albums and average days high hours low minutes months of photo photo albums. The last album added is photos seconds very high very low weeks years Project-Id-Version: WP Photo Album Plus in italiano
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-09-18 17:44+0100
PO-Revision-Date: 2011-09-22 19:17+0100
Last-Translator: Gianni Diurno (aka gidibao) <gidibao[at]gmail[dot]com>
Language-Team: Gianni Diurno | gidibao.net & charmingpress.com
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __a
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: utf-8
X-Poedit-Language: Italian
X-Poedit-Country: ITALY
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: theme
 %d  commenti , un album-figlio di - - - Casella commenti attiva - - - - - - Valutaz. attiva - - - --- nessuno --- --- separato --- 1 giorno 1 ora 1 minuto 1 mese 1 secondo 1 settimana 1 anno <b>ERRORE: Tentativo inserimento voto non valido.</b> <b>ERRORE: Tentativo inserimento non valido commento.</b> <b>ERRORE: Tentativo non valido inserimento voto.</b> <b>ERRORE: Tentativo non valido caricamento file.</b> Voto&nbsp;medio Naviga foto Clicca per avvio/stop Il commento è stato aggiunto Il commento è stato modificato Impossibile processare il commento Doppio click per avvio/stop riproduzione slideshow Modifica! Inserisci/modifica descrizione foto Si è verificato un errore durante il caricamento + veloce Home Immagine Lascia un commento Link a Mio&nbsp;voto Succ. Foto successiva Pagina&nbsp;succ. Nessun album definito. Nessun album o foto corrispondente ai criteri di ricerca. Sono supportati i soli formati immagine gi, jpg e png Pagina non disponibile. Foto Foto del Giorno Foto non trovata. Inserisci un commento Inserisci un indirizzo email valido Inserisci il tuo nome Prec. Pagina&nbsp;prec. Foto precedente Voto: %s Stringa di ricerca: Invia! Slideshow barra laterale Slideshow + lento Avvio Stop Ci sono Per le immagini a dimensione intera, attivare javascript nel proprio browser. Foto Top Ten Le foto più votate Carica foto Il file caricato non è una immagine Vedi Vedi album Vedi copertina foto Vedi copertina fotografie Vedi le foto più votate Login per effettuare un commento Necessario login per votare Il tuo commento: La tua email: Il tuo nome: album Album e discreto giorni alto ore basso minuti mesi di foto album di foto. L'ultimo album aggiunto è Foto secondi molto alto molto basso settimane anni 