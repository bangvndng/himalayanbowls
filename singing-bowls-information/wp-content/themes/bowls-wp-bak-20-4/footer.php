<?php
/**
 * @package WordPress
 * @subpackage Classic_Theme
 */
?>
    </div>
    <div class="him-white-shadow"><!--  --></div>

    <div class="him-footer clearfix">
        <div class="panel">
            <h3>Himalayan Bowls</h3>
            <ul>
    			<li><a href="/antique-singing-bowls.html">Antique Singing Bowls</a></li>         
    			<li><a href="/new-singing-bowls.html">New Singing Bowls</a></li>         
    			<li><a href="/singing-bowl-accessories.html">Singing Bowl Accessories</a></li>         
    			<li><a href="/singing-bowl-cds-video-books.html">Audio &amp; Books</a></li>         
    			<li><a href="/tibetan-instruments-buddhist-art.html">Tibetan Instruments</a></li>         	
            </ul>
        </div>
        <div class="panel">
            <h3>Customer Service</h3>
            <ul>
                <li><a href="/about-himalayan-bowls">About Himalayan Bowls</a></li>
                <li><a href="/customer-quotes">Customer Quotes</a></li>
                <li><a href="/customer-service">Customer Service</a></li>
                <li><a href="/contacts">Contact</a></li>
                <li><a href="/singing-bowls-information">More Information Pages</a></li>
            </ul>
        </div>
        <div class="panel">
            <h3>Question about singing bowls?</h3>
            <p>Click the contact link to send us an email. Or call</p>
            <h1 style="color:#ffffff; margin-bottom:6px; font-size:24px;">415-386-6683</h1>
            Mon - Sat: 10AM - 5PM PST
        </div>
        <div class="panel-last">
            <h3>Social Links</h3>
            <table cellpadding="0" cellspacing="" class="social">
              <tr>
                <!--<td><a href="#"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icon-linkedin.png" width="32" height="32" alt="" /></a><br/>LinkedIn</td>-->
                <td><a target="_blank" href="http://www.facebook.com/HimalayanBowls"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icon-facebook.png" width="32" height="32" alt="" /></a><br/>Facebook</td>
                <td><a target="_blank" href="http://www.youtube.com/user/himalayanbowls"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icon-youtube.png" width="32" height="32" alt="" /></a><br/>Youtube</td>
                <td><a target="_blank" href="http://twitter.com/HimalayanBowls"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icon-twitter.png" width="32" height="32" alt="" /></a><br/>Twitter</td>
                <td><a target="_blank" href="http://www.myspace.com/474852125"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icon-myspace.png" width="32" height="32" alt="" /></a><br/>Myspace</td>
                <td  class="st_sharethis" ><a target="_blank" href="javascript:void(0)"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/icon-share.png" width="32" height="32" alt="" /></a><br/>Share</td>
              </tr>
            </table>

            <h3 class="mb0">Subscribe to Newsletter</h3>
            <table cellpadding="0" cellspacing="0">
              <tr>
                <td><input name="Subscribe" style="width:220px;" value="Your Email..." onfocus="javascript:if(this.value=='Your Email...') this.value=''" onblur="if(this.value=='') this.value='Your Email...'"/></td>
                <td style="padding-top:4px; padding-left:6px;"><input type="image" name="go" src="<?php bloginfo('stylesheet_directory'); ?>/images/button-go.png" alt="Go" /></td>
              </tr>
            </table>
        </div>
        <div class="clear"></div>
        <div class="separator"><!--  --></div>
        <div class="copy clearfix">
            <div class="fl">Copyright &copy; 2010 Himalayan Bowls. All Rights Reserved.</div>
            <!--<div class="fr">website: <a href="http://www.transformagency.com/" rel="external">Transform Agency</a></div>-->
        </div>
    </div>

</div>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">
        stLight.options({
                publisher:'1b109c67-0dbc-4fe4-90ec-38153d94a2e4',
        });
</script>   
<script type="text/javascript"> Cufon.now(); </script>
</body>
</html>