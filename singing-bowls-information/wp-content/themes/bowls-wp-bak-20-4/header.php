<?php
/**
 * @package WordPress
 * @subpackage Classic_Theme
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
	<style type="text/css" media="screen">
		@import url( <?php bloginfo('stylesheet_url'); ?> );
	</style>
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_get_archives('type=monthly&format=link'); ?>
	<?php //comments_popup_script(); // off by default ?>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript"></script>

    <!--[if lt IE 7]>
        <script type="text/javascript" src="<?php bloginfo('stylesheet_directory'); ?>/js/pngfix.js"></script>
        <script type="text/javascript">DD_belatedPNG.fix('*');</script>
    <![endif]-->

    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/style.css" type="text/css" media="screen" />
	<!--
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/cufon.js" type="text/javascript"></script> -->
	<script src="http://cufon.shoqolate.com/js/cufon-yui.js?v=1.09i" type="text/javascript"></script>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/Proxima_400-Proxima_600-Proxima_italic_400-Proxima_italic_600.font.js" type="text/javascript"></script>

	<script type="text/javascript">
            Cufon.replace('h1, h2, h3, h4, .him-top-menu ul li, .regular-price, .button, .button-big, .note', { hover: true, fontFamily: 'Proxima' });
            Cufon.replace('.button1, .button1-big', { textShadow: '1px 1px 0 #a43815', fontFamily: 'Proxima' });
            Cufon.replace('.him-announce', { fontFamily: 'Proxima' });
    </script>

    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/prettyPhoto.css" type="text/css" media="screen" />
    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script type="text/javascript">
      jQuery(document).ready(function() {
        $("a[rel^='prettyPhoto']").prettyPhoto();
      });
    </script>

    <link rel="shortcut icon" href="<?php bloginfo('stylesheet_directory'); ?>/favicon.ico" type="image/x-icon" />

    <script src="<?php bloginfo('stylesheet_directory'); ?>/js/superfish.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $.get('/checkout/cart/ajax',function(data){
            $('#ajax_cart').html(data);
         });
		//Main navigation dropdowns
        $('ul.sf-menu').superfish({
            delay:       900,                            // one second delay on mouseout
            animation:   { height:'show' },  // fade-in and slide-down animation
            speed:       'fast',                          // faster animation speed
            autoArrows:  false,                           // disable generation of arrow mark-up
            dropShadows: false                            // disable drop shadows
        });
     });
	function popWin(url,win,para) {
		var win = window.open(url,win,para);
		win.focus();
	}
    </script>

</head>

<body>

<div class="him-contain">
    <div class="him-white clearfix">

        <div class="him-logo"><a href="/index.php"><img src="<?php bloginfo('stylesheet_directory'); ?>/images/hym-logo.png" width="279" height="67" alt="" /></a></div>
        <div class="him-top-menu">
            <ul class="clearfix">
                <li><a href="/">Home</a></li>
                <li><a href="/customer/account">My Account</a></li>
                <li><a href="#" onclick="popWin('/catalog/product_compare/index/','compare','top:0,left:0,width=820,height=600,resizable=yes,scrollbars=yes')" >Compare Products</a></li>
                <li><a href="/checkout/cart/">View Cart</a></li>
                <li><a href="/customer-service">Customer Service</a></li>
                <li class="last"><a href="/contacts">Contact Us</a></li>
            </ul>
            <div class="clear"></div>
            <div class="him-sect">
              <table cellpadding="0" cellspacing="0">
                <tr>
                  <td class="him-reg">
                    <!--<a href="#"><img src="<?=$img_dir?>/flag-us.jpg" width="24" height="14" alt="" /></a>&nbsp;&nbsp;<a href="#"><img src="<?=$img_dir?>/flag-german.jpg" width="24" height="14" alt="" /></a>&nbsp;&nbsp;
                    Currency : <select><option>USD</option><option>Euro</option></select>-->
                  </td>
                  <td id="ajax_cart">                     
                  </td>
                </tr>
              </table>
            </div>
        </div>
        <div class="clear"></div>

        <div class="him-announce">Sounds of Enlightenment - Finest Quality Tibetan Singing Bowls & Tibetan Instruments</div>
