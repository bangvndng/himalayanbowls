<?php
/**
 * @package WordPress
 * @subpackage Classic_Theme
 */

if ( post_password_required() ) : ?>
<p><?php _e('Enter your password to view comments.'); ?></p>
<?php return; endif; ?>

<h2 id="comments" style="margin-top:8px;margin-bottom:10px;"><?php comments_number(__('No Comments'), __('1 Comment'), __('% Comments')); ?>
<?php if ( comments_open() ) : ?>
	<a href="#postcomment" title="<?php _e("Leave a comment"); ?>"></a>
<?php endif; ?>
</h2>

<?php if ( have_comments() ) : ?>
<?php foreach ($comments as $comment) : ?>
	<div <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">
	<?php echo get_avatar( $comment, 32 ); ?>
	<?php comment_text() ?>
	<p><cite><?php comment_type(_x('Comment', 'noun'), __('Trackback'), __('Pingback')); ?> <?php _e('by'); ?> <?php comment_author_link() ?> &#8212; <?php comment_date() ?> @ <a href="#comment-<?php comment_ID() ?>"><?php comment_time() ?></a> <?php edit_comment_link(__("Edit This"), ' | '); ?></cite></p>
	</div>

<?php endforeach; ?>

<?php else : // If there are no comments yet ?>
	<p><?php _e('No comments yet.'); ?></p>
<?php endif; ?>

<?php if ( comments_open() ) : ?>
<div class="articleshort" style="padding:14px 18px 10px 0px">
<h2 id="postcomment" style="padding-bottom:7px;margin-bottom:5px;border-bottom:1px solid #dfdfdf"><?php _e('Leave a comment'); ?></h2>

<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
<p><?php printf(__('You must be <a href="%s">logged in</a> to post a comment.'), wp_login_url( get_permalink() ) );?></p>
<?php else : ?>

<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

<?php if ( is_user_logged_in() ) : ?>

<p><?php printf(__('Logged in as %s.'), '<a href="'.get_option('siteurl').'/wp-admin/profile.php">'.$user_identity.'</a>'); ?> <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e('Log out of this account') ?>"><?php _e('Log out &raquo;'); ?></a></p>

<?php else : ?>

<p style="padding:5px 0px 5px 0px;margin:0px;"><input type="text" name="author" class="input" id="author" value="<?php echo esc_attr($comment_author); ?>" size="42" tabindex="1" />
<label for="author"><?php _e('&nbsp;Name'); ?> <?php if ($req) _e('(required)'); ?></label></p>
<div style="clear:both"><!-- --></div>
<p style="padding:0px 0px 5px 0px;margin:0px;"><input type="text" name="email" class="input" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="42" tabindex="2" />
<label for="email"><?php _e('&nbsp;Mail (will not be published)');?> <?php if ($req) _e('(required)'); ?></label></p>
<div style="clear:both"><!-- --></div>
<p style="padding:0px 0px 5px 0px;margin:0px;"><input type="text" name="url" class="input" id="url" value="<?php echo esc_attr($comment_author_url); ?>" size="42" tabindex="3" />
<label for="url"><?php _e('&nbsp;Website'); ?></label></p>
<div style="clear:both"><!-- --></div>

<?php endif; ?>

<!--<p><small><strong>XHTML:</strong> <?php printf(__('You can use these tags: %s'), allowed_tags()); ?></small></p>-->

<p><textarea name="comment" class="input" id="comment" rows="10" tabindex="4" style="width:670px"></textarea></p>

<div style="padding-bottom:8px;"><button name="submit" id="submit" tabindex="5" class="button1" value="<?php esc_attr_e('Submit Comment'); ?>" /><?php esc_attr_e('Submit Comment'); ?></button>
<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
</div>
<?php do_action('comment_form', $post->ID); ?>

</form>
<?php endif; // If registration required and not logged in ?>
</div>

<?php else : // Comments are closed ?>
<p><?php _e('Sorry, the comment form is closed at this time.'); ?></p>
<?php endif; ?>
