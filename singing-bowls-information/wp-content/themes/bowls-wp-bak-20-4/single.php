<?php
/**
 * @package WordPress
 * @subpackage Classic_Theme
 */
get_header();
?>

        <div class="him-maincol blog-single">

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            	<h2><?php the_title(); ?></h2>
            	<div class="byauthor"><?php the_tags('Tags: ', ', ', '<br />'); ?>Posted in <?php the_category(', ') ?> by <?php the_author() ?> on <?php the_time('F jS, Y') ?> <?php edit_post_link(__('Edit This')); ?></div>
                    
            	<div class="storycontent">
            		<?php the_content(); ?>
            	</div>
            <?php comments_template(); // Get wp-comments.php template ?>

            <?php endwhile; else: ?>
            <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
            <?php endif; ?>

            <?php posts_nav_link(' &#8212; ', __('&laquo; Newer Posts'), __('Older Posts &raquo;')); ?>

    		<div style="padding:4px 0px 20px 0px">
    			<div style="float:left"><?php previous_posts_link('&laquo; Newer Articles') ?></div>
    			<div style="float:right"><?php next_posts_link('Older Articles  &raquo;') ?></div>
    			<div class="clear"><!-- --></div>
    		</div>

        </div>

        <div class="him-sidebar">
            <?php get_sidebar(); ?>
        </div>
        <div class="clear"></div>


<?php get_footer(); ?>
