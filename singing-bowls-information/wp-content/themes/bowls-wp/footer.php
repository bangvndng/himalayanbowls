<?php
/**
 * @package WordPress
 * @subpackage Classic_Theme
 */
?>
</div>


    <div class="him-footer clearfix">
        <div class="footer">
            <div class="panel-first">
                <h3>Himalayan Bowls</h3>
                <ul>
                    <li><a href="http://himalayanbowls.com/antique-singing-bowls.html">Antique Singing Bowls</a></li>
                    <li><a href="http://himalayanbowls.com/new-singing-bowls.html">New Bronze Singing Bowls</a></li>
                    <li><a href="http://himalayanbowls.com/singing-bowl-cds-video-books.html">Singing Bowl Recordings</a></li>

                </ul>
            </div>
            <div class="panel">
                <h3>Customer Service</h3>
                <ul>
                    <li><a href="http://himalayanbowls.com/about-himalayan-bowls">About Himalayan Bowls</a></li>
                    <li><a href="http://himalayanbowls.com/customer-quotes">Customer Quotes</a></li>
                    <li><a href="http://himalayanbowls.com/customer-service">Customer Service</a></li>
                    <li><a href="http://himalayanbowls.com/contacts">Contact</a></li>
                    <li><a href="http://himalayanbowls.com/singing-bowls-information" target="_blank">More Information Pages</a></li>
                </ul>
            </div>
            <div class="panel">
                <div class="contact">
                    <p>Questions about singing bowls?</p>
                    <button class="contact-button" onclick="setLocation('contacts')"> <span><span>Contact</span></span> </button></div>            <div class="home-new-letter">
                    <p>Please join our mailing list</p>
                    <form action="https://local.himalayanbowls.com/newsletter/subscriber/new/" method="post" id="newsletter-validate-detail">
                        <table cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                    <td class="input"><input type="text" name="email" id="newsletter" title="Join our mailing list to learn singing bowl secrets" class="input-text required-entry validate-email" value="Your Email..." onfocus="javascript:if(this.value=='Your Email...') this.value=''" onblur="if(this.value=='') this.value='Your Email...'"></td>
                                    <td class="submit"><input type="submit" name="go" alt="Go" value="GO"></td>
                                </tr>
                            </tbody></table>
                    </form>
                    <script type="text/javascript">
                        //<![CDATA[
                        var newsletterSubscriberFormDetail = new VarienForm('newsletter-validate-detail');
                        //]]>
                    </script>
                </div>
            </div>
            <div class="panel-last">
                <div class="home-social-share">
                    <h3>Social Links</h3>
                    <table class="social" cellpadding="0">
                        <tbody>
                            <tr>
                                <td><a href="http://www.facebook.com/HimalayanBowls" target="_blank"><img src="http://himalayanbowls.com/skin/frontend/bowls/new2012/images/icon-facebook.png" alt="" width="32" height="32"></a><br></td>
                                <td><a href="http://www.youtube.com/user/himalayanbowls" target="_blank"><img src="http://himalayanbowls.com/skin/frontend/bowls/new2012/images/icon-youtube.png" alt="" width="32" height="32"></a><br></td>
                                <td><a href="http://twitter.com/HimalayanBowls" target="_blank"><img src="http://himalayanbowls.com/skin/frontend/bowls/new2012/images/icon-twitter.png" alt="" width="32" height="32"></a><br></td>
                                <td><a href="http://www.myspace.com/474852125" target="_blank"><img src="http://himalayanbowls.com/skin/frontend/bowls/new2012/images/icon-myspace.png" alt="" width="32" height="32"></a><br></td>
                            </tr>
                        </tbody>
                    </table>            </div>
                <div class="bank-card">
                    <div class="b-bank"><a id="bbblink" class="rbvtbum" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; " title="Himalayan Bowls is a BBB Accredited Importer in San Francisco, CA" href="http://www.bbb.org/greater-san-francisco/business-reviews/importers/himalayan-bowls-in-san-francisco-ca-381158#bbblogo" target="_blank"><img id="bbblinkimg" style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; border-top-style: none; border-right-style: none; border-bottom-style: none; border-left-style: none; border-width: initial; border-color: initial; border-image: initial; " src="http://seal-goldengate.bbb.org/logo/rbvtbum/himalayan-bowls-381158.png" alt="Himalayan Bowls is a BBB Accredited Importer in San Francisco, CA" width="120" height="98"></a>
                        <script type="text/javascript">// <![CDATA[
                            var bbbprotocol = ( ("https:" == document.location.protocol) ? "https://" : "http://" ); document.write(unescape("%3Cscript src='" + bbbprotocol + 'seal-goldengate.bbb.org' + unescape('%2Flogo%2Fhimalayan-bowls-381158.js') + "' type='text/javascript'%3E%3C/script%3E"));
                            // ]]></script><script src="http://seal-goldengate.bbb.org/logo/himalayan-bowls-381158.js" type="text/javascript"></script>
                    </div>
                    <div class="paypal"><a href="#">paypal</a></div>
                    <div class="authorize">
                        <div class="AuthorizeNetSeal">
                            <script type="text/javascript">// <![CDATA[
                                var ANS_customer_id="acd0762d-d89d-4882-a9fe-70bbef1ed620";
                                // ]]></script>
                            <script src="//verify.authorize.net/anetseal/seal.js" type="text/javascript"></script><style type="text/css">
                                div.AuthorizeNetSeal{text-align:center;margin:0;padding:0;width:90px;font:normal 9px arial,helvetica,san-serif;line-height:10px;}
                                div.AuthorizeNetSeal a{text-decoration:none;color:black;}
                                div.AuthorizeNetSeal a:visited{color:black;}
                                div.AuthorizeNetSeal a:active{color:black;}
                                div.AuthorizeNetSeal a:hover{text-decoration:underline;color:black;}
                                div.AuthorizeNetSeal a img{border:0px;margin:0px;text-decoration:none;}
                            </style>
                            <a href="//verify.authorize.net/anetseal/?pid=acd0762d-d89d-4882-a9fe-70bbef1ed620&amp;rurl=http%3A//local.himalayanbowls.com/antique-singing-bowls.html" onmouseover="window.status='http://www.authorize.net/'; return true;" onmouseout="window.status=''; return true;" onclick="window.open('//verify.authorize.net/anetseal/?pid=acd0762d-d89d-4882-a9fe-70bbef1ed620&amp;rurl=http%3A//local.himalayanbowls.com/antique-singing-bowls.html','AuthorizeNetVerification','width=600,height=430,dependent=yes,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,status=no,directories=no,location=yes'); return false;" target="_blank">
                                <img src="//verify.authorize.net/anetseal/images/secure90x72.gif" width="90" height="72" border="0" alt="Authorize.Net Merchant - Click to Verify">
                            </a>

                            <a id="AuthorizeNetText" href="http://www.authorize.net/" target="_blank">Online Payments</a></div>
                    </div>
                </div>
            </div>
            <div class="copy clearfix">
                <div class="fl">Copyright © 2010 Himalayan Bowls. All Rights Reserved.</div>
            </div>
        </div>

    </div>
    <div class="him-white-shadow"><!--  --></div>
<?php /*
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">
    stLight.options({
        publisher:'1b109c67-0dbc-4fe4-90ec-38153d94a2e4',
    });
</script> */?>
<script type="text/javascript"> Cufon.now(); </script>
</body>
</html>