<?php
/**
 * @package WordPress
 * @subpackage Classic_Theme
 */
?>
<!-- begin sidebar -->

            <div class="him-search">
                <form id="searchform" method="get" action="<?php bloginfo('home'); ?>">
                <table cellpadding="0" cellspacing="0">
                  <tr>
                    <td><input name="s" id="s" class="searchfield" value="Blog Search..." onfocus="javascript:if(this.value=='Blog Search...') this.value=''" onblur="if(this.value=='') this.value='Blog Search...'"/></td>
                    <td style="padding-top:2px; padding-left:5px;"><input type="image" name="Go" src="<?php bloginfo('stylesheet_directory'); ?>/images/button-go.png" alt="" value="<?php esc_attr_e('Search'); ?>"/></td>
                  </tr>
                </table>
                </form>
            </div>

            <div class="him-boxed blog-side-list">
            <ul>
                <?php 	/* Widgetized sidebar, if you have the plugin installed. */
                		if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar() ) : ?>
                          <?php wp_list_categories('title_li=' . __('<h2>Categories</h2>')); ?>
                           <li id="archives"><?php _e('<h2>Archives</h2>'); ?>
                          	<ul>
                          	 <?php wp_get_archives('type=monthly'); ?>
                          	</ul>
                           </li>
                          <?php endif; ?>
             </ul>
            </div>
<!-- end sidebar -->
