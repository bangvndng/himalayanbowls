<?php
/**
 * @package WordPress
 * @subpackage Classic_Theme
 */
get_header();
?>


        <div class="him-maincol">

        		<?php if (have_posts()) : ?>

         	  <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
         	  <?php /* If this is a category archive */ if (is_category()) { ?>
        		<h2 class="archive-heading"><?php single_cat_title(); ?></h2>
         	  <?php } ?>

        		<?php while (have_posts()) : the_post(); ?>

        <div class="blog-post archive-post">
             <h2 class="mb0"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
        	 <div class="blog-post-content">
        		<p class="meta"><?php _e("Filed under:"); ?> <?php the_category(',') ?> &#8212; <?php the_tags(__('Tags: '), ', ', ' &#8212; '); ?> <?php the_author() ?> @ <?php the_time() ?> <?php edit_post_link(__('Edit This')); ?> &nbsp;|&nbsp; <img src="<?php bloginfo('stylesheet_directory'); ?>/images/icon-comment.png" width="16" height="16" alt="" class="vm"/> <?php comments_popup_link(__('Comments (0)'), __('Comments (1)'), __('Comments (%)')); ?></p>
        		<?php the_excerpt(); ?>
        		<div class="blog-post-more"><a href="<?php the_permalink() ?>">Read More &raquo;</a></div>
        	</div>
        	<div class="feedback">
        		<?php wp_link_pages(); ?>
        	</div>
        </div>
        <?php comments_template(); // Get wp-comments.php template ?>
        <?php endwhile; else: ?>
        <h3>Sorry, No Results</h3><div style="border-top:1px solid #dbdbdb;border-bottom:1px solid #dbdbdb;padding:20px 0px 20px 0px;text-align:center"><?php _e('Sorry, no posts matched your criteria.'); ?><br /><a href="<?php bloginfo('url'); ?>">Go back</a> to blog homepage</div>
        <?php endif; ?>

        		<div style="padding:4px 0px 20px 0px">
        			<div style="float:left"><a href="<?php bloginfo('url'); ?>">&laquo; Back to blog homepage</a></div>
        			<div style="float:right"><?php previous_posts_link('&laquo; Newer Articles') ?><?php next_posts_link('&nbsp;&nbsp;&nbsp;&nbsp;Older Articles  &raquo;') ?></div>
        			<div class="clear"><!-- --></div>
        		</div>

        </div>

        <div class="him-sidebar">
            <?php get_sidebar(); ?>
        </div>

        <div class="clear"></div>


<?php get_footer(); ?>
