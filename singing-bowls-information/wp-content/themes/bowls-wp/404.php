<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<div class="main-content">
    <div class="containit">

        <!--<div class="breadcrumbs">
            <div class="fl">You are here:&nbsp;&nbsp; <a href="#">Home</a> <img src="<?=$img_dir?>/arrow-breadcrumb.png" width="3" height="5" alt="" class="vm barrow"/> Blog</div>
            <div class="fr"><a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4c62fb625779dbe4"><img src="<?=$img_dir?>/icon-share.png" width="16" height="16" alt="" class="vm"/> Share This Page</a><script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4c62fb625779dbe4"></script></div>
            <div class="clear"></div>
        </div>

        <div class="wide-horz-divider">&nbsp;</div>-->

        <div class="col1 border-vert-right">

<h3>Page Not Found</h3><div style="border-top:1px solid #dbdbdb;border-bottom:1px solid #dbdbdb;padding:20px 0px 20px 0px;text-align:center"><?php _e( 'Apologies, but the page you requested could not be found.', 'twentyten' ); ?><br /><a href="<?php bloginfo('url'); ?>">Go back</a> to blog homepage</div>

        </div>
        <div class="col2">
            <?php get_sidebar(); ?>
        </div>
        <div class="clear"></div>

    </div>
</div>


<?php get_footer(); ?>