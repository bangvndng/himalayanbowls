<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hb_magento');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', 'fe3be421');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i,EQO#e%iB01^Q^>DA.+|hdpK=1G{r! [=|1sOV}jRTE=d4)<%M,}]M&I|sv_e-m');
define('SECURE_AUTH_KEY',  '^{t:d!qww-%s7XHP={fY;b?1-5^V9m{b2xhcex/}?0XW0RU`.i|f;aJPG.epzQH ');
define('LOGGED_IN_KEY',    'j`6h4qyX?qPDP?7nY|8S<:N=b+`>4_RZ->$sA[3#Re/; z)UCV|++6v-:Q$d_6[U');
define('NONCE_KEY',        'Pge+#UWWu+Md-hUQ2hz4!hHBn5Y@qgRa|^>eO!V6JThi7dRILhKE$`&Bn{9w17R{');
define('AUTH_SALT',        'e9f!Jbu4}*73/x QBa$KwS[_u6`F U+2rJsY&|(ZAhcq;/I#SRJD*ojdFQ;lVTN9');
define('SECURE_AUTH_SALT', '1v.CyeV6|E[%3-4n=r5+@&!VeQ??]+r}[*WD2)Gvp$Do}1jj`*ych;Ek(%vQ]^xl');
define('LOGGED_IN_SALT',   'R j0m+m(= (|_]Q-P`qO!)5[Yy=4Z`ii}:<#ib8Tx]S-:e2vfkJSi>kl.tY36|Ge');
define('NONCE_SALT',       'x,;o*&XY.2@Ide+{v!AqR+D)Ibe0X@jInMO3.Kyx:yL bmuNw4`FS:E8@.H*G9Q>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress.  A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define ('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
