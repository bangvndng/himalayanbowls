<?php
/**
 * MAGENTO - RUSH ORDERS POST SYSTEM
 * GREEN GLOBAL Co.,ltd
 *
 * @author bangvn
 * @link bangvn@toancauxanh.vn
 *
 * @uses this file is used when a cron run.
 */

require_once 'app/Mage.php';



if (!Mage::isInstalled()) {
        Mage::log("Application is not installed yet, please complete install wizard first.");
        exit;
}

// Only for urls
// Don't remove this
$_SERVER['SCRIPT_NAME'] = str_replace(basename(__FILE__), 'index.php', $_SERVER['SCRIPT_NAME']);
$_SERVER['SCRIPT_FILENAME'] = str_replace(basename(__FILE__), 'index.php', $_SERVER['SCRIPT_FILENAME']);

Mage::app('admin')->setUseSessionInUrl(false);

try {
        require_once 'repost.php';
        repostcronXML();
        //sendAlertEmail("Himalayanbowls - Rush Order Repost System Cronjob Success", "Reposting Cron is running now");
} catch (Exception $e) {
        sendAlertEmail("Himalayanbowls - Rush Order Repost System Cronjob Failed", "Reposting Cron is failed to run");
        Mage::printException($e);
        Mage::logException($e);
}

function sendAlertEmail($subject, $body) {
        $fromEmail = "support@himalayanbowls.com"; // sender email address
        $fromName = "Himalayanbowls Administrator"; // sender name

        $toEmail1 = "joseph@himalayanbowls.com"; // recipient email address
        $toName1 = "Joseph"; // recipient name

        $toEmail2 = "bangvn@webdev.vn"; // recipient email address
        $toName2 = "Bang Vu Ngoc"; // recipient name

        $mail = new Zend_Mail();
        $mail->setBodyText($body);
        $mail->setFrom($fromEmail, $fromName);
        //$mail->addTo($toEmail1, $toName1);
        //$mail->addTo($toEmail2, $toName2);
        $mail->setSubject($subject);
        try {
                $mail->send();
        } catch (Exception $ex) {
                Mage::printException($e);
                Mage::logException($e);
        }
}
