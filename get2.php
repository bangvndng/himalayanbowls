<?php

require_once 'app/Mage.php';
Mage::app('default');

Mage::log('some'); die;
?>
<?php

if (isset($_GET) && !empty($_GET)) {
    if (isset($_GET['orderId']) && !empty($_GET['orderId'])) {
        if (isset($_GET['trackingNumber']) && !empty($_GET['trackingNumber'])) {
            doShipment($_GET['orderId'], $_GET['trackingNumber']);
        }
    }
}
?>
<?php

$orders = Mage::getResourceModel('sales/order_collection')
        ->addAttributeToSelect('*')
        ->addFieldToFilter('status', array("in" => array('processing')))
        ->addAttributeToFilter('store_id', Mage::app()->getStore()->getId())
        ->addAttributeToSort('created_at', 'asc')
        ->load();
$ping_orders = array();
$ping_orders_string = '';
foreach ($orders as $order):
    $ping_orders[] = $order->getIncrementId();
endforeach;
$ping_orders_string = implode(",", $ping_orders);
//print_r($ping_orders);
print_r($ping_orders_string);
echo '<br>';

$target_url = "https://tools.rushorder.com/wizmo2/status.cfm?code=HYC&altorder=".$ping_orders_string;
print_r($target_url);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $target_url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

//curl_setopt($ch, CURLOPT_POST, 1);
//curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
//curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);

$result = curl_exec($ch);

print_r($result);

curl_close($ch);
?>
<?php

function doShipment($orderId, $trackNumber) {
    $carrier = "usps";
    $title = "Rush Order Shipping";
    $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
    if ($order->canShip()) {
        $itemQty = $order->getItemsCollection()->count();
        $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment($itemQty);
        $shipment = new Mage_Sales_Model_Order_Shipment_Api();
        try {
            $shipmentId = $shipment->create($orderId);
            $shipment->addTrack($shipmentId, $carrier, $title, $trackNumber);
        } catch (Exception $e) {
            Mage::log($e, 'response_from_rushorder');
        }
    }
}
?>