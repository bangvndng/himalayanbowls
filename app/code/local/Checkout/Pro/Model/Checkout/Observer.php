<?php

class Checkout_Pro_Model_Checkout_Observer {

    public function collectTotalBefore($observer) {
        Mage::getSingleton('core/session')->setRateInfo(null);
    }

    public function taxFetch($observer) {
        $request = $observer->getRequest();
        $rateInfo = Mage::getModel('tax/mysql4_calculation')->getRateInfo($request);
//		Mage::log($rateInfo);
        $process = array();
        foreach ($rateInfo['process'] as $info) {
            unset($info['rates']);
            array_push($process, $info);
        }
        $rateInfo['process'] = $process;
        $tax = array();
        if (!Mage::getSingleton('core/session')->getRateInfo()) {
            $tax = $rateInfo['process'];
        } else {
            $tax1 = Mage::getSingleton('core/session')->getRateInfo();
            array_push($tax, $tax1);
            array_push($tax, $rateInfo['process']);
        }
        Mage::getSingleton('core/session')->setRateInfo($tax);
    }

    public function checkoutSucess($observer) {

        $enabled = true;
        $config = Mage::getStoreConfig('pro/checkout/enable');
        if (!empty($config)) {
            if (!$config) {
                return;
            }
        }
        $order = $observer->getOrder();
        $quote = $observer->getQuote();

        $orderArr = array();

        $info = array();
        $info['store_name'] = Mage::getStoreConfig('general/store_information/name', Mage::app()->getStore()->getId());
        $info['store_telephone'] = Mage::getStoreConfig('general/store_information/phone', Mage::app()->getStore()->getId());
        $info['store_address'] = Mage::getStoreConfig('general/store_information/address', Mage::app()->getStore()->getId());
        $info['http_server'] = Mage::getUrl();


        $emailArray = array();

        $emailtype = array();
        $emailtype['email_type'] = 'Customer Support';
        $emailtype['email_name'] = Mage::getStoreConfig('trans_email/ident_support/name', Mage::app()->getStore()->getId());
        $emailtype['email_addr'] = Mage::getStoreConfig('trans_email/ident_support/email', Mage::app()->getStore()->getId());
        $emailArray[] = $emailtype;

        $emailtype = array();
        $emailtype['email_type'] = 'Sales representative';
        $emailtype['email_name'] = Mage::getStoreConfig('trans_email/ident_sales/name', Mage::app()->getStore()->getId());
        $emailtype['email_addr'] = Mage::getStoreConfig('trans_email/ident_sales/email', Mage::app()->getStore()->getId());
        $emailArray[] = $emailtype;

        $emailtype = array();
        $emailtype['email_type'] = 'General Contact';
        $emailtype['email_name'] = Mage::getStoreConfig('trans_email/ident_general/name', Mage::app()->getStore()->getId());
        $emailtype['email_addr'] = Mage::getStoreConfig('trans_email/ident_general/email', Mage::app()->getStore()->getId());
        $emailArray[] = $emailtype;

        $collection = Mage::getModel('admin/user')->getCollection();
        foreach ($collection as $user) {
            $emailtype = array();
            $role = $user->getRole();
            if ($role->getRoleType() == 'G') {
                $emailtype['email_type'] = 'Admin';
                $emailtype['email_name'] = $user->getFirstname() . ' ' . $user->getLastname();
                $emailtype['email_addr'] = $user->getEmail();
                $emailArray[] = $emailtype;
            }
        }

        $customer = Mage::getSingleton('customer/session');
        $customer = Mage::getModel('customer/customer')->load($customer->getCustomer()->getId());

        if ($customer) {
            $customerArr = array();
            $customerArr['customers_id'] = $customer->getEntityId();
            $customerArr['customers_gender'] = $customer->getGender();
            $customerArr['customers_firstname'] = $customer->getFirstname();
            $customerArr['customers_lastname'] = $customer->getLastname();
            $customerArr['customers_dob'] = $customer->getDob();
            $customerArr['customers_email_address'] = $customer->getEmail();
            $customerArr['customers_telephone'] = $customer->getTelephone();
            $customerArr['customers_fax'] = $customer->getFax();
            $customerArr['customers_taxvat'] = $customer->getTaxvat();
        }

        $orders = array();
        $orders['orders_id'] = $order->getId();

        $address = $quote->getShippingAddress();
        $orders['delivery_id'] = $address->getEntityId();
        $orders['delivery_firstname'] = '<![CDATA[' . $address->getFirstname() . ']]>';
        $orders['delivery_lastname'] = '<![CDATA[' . $address->getLastname() . ']]>';
        $orders['delivery_company'] = '<![CDATA[' . $address->getCompany() . ']]>';
        $orders['delivery_street_address'] = '<![CDATA[' . implode(',', $address->getStreet()) . ']]>';
        $orders['delivery_city'] = $address->getCity();
        $orders['delivery_postcode'] = $address->getPostcode();
        $orders['delivery_state'] = $address->getRegion();
        $orders['delivery_country'] = $address->getCountryId();
        $orders['delivery_region_code'] = $address->getRegionCode();
        $orders['delivery_email'] = $address->getEmail();
        $orders['delivery_telephone'] = $address->getTelephone();

        $address = $quote->getBillingAddress();
        $orders['billing_id'] = $address->getEntityId();
        $orders['billing_firstname'] = '<![CDATA[' . $address->getFirstname() . ']]>';
        $orders['billing_lastname'] = '<![CDATA[' . $address->getLastname() . ']]>';
        $orders['billing_company'] = '<![CDATA[' . $address->getCompany() . ']]>';
        $orders['billing_street_address'] = '<![CDATA[' . implode(',', $address->getStreet()) . ']]>';
        $orders['billing_city'] = $address->getCity();
        $orders['billing_postcode'] = $address->getPostcode();
        $orders['billing_state'] = $address->getRegion();
        $orders['billing_country'] = $address->getCountryId();
        $orders['billing_region_code'] = $address->getRegionCode();
        $orders['billing_email'] = $address->getEmail();
        $orders['billing_telephone'] = $address->getTelephone();

        $paymentmethod = "";
        if ($quote->isVirtual()) {
            $paymentmethod = $quote->getBillingAddress()->getPaymentMethod();
            Mage::log("Virtual MP3 Product, skipping");
            Mage::log(date('Y-m-d H:i:s'));
            Mage::log("Order ID : " . $orders['orders_id']);
            return;
        } else {
            $paymentmethod = $quote->getShippingAddress()->getPaymentMethod();
        }
        $orders['payment_method'] = $paymentmethod;


        $shippingmethod = $quote->getShippingAddress()->getShippingMethod();

        $orders['shipping_method'] = $shippingmethod;

        $orders['date_purchased'] = date('Y-m-d H:i:s');
        $orders['orders_status'] = $order->getStatusLabel();

        $orders['orders_id'] = $order->getIncrementId();

        $orders['currency'] = Mage::app()->getStore()->getCurrentCurrencyCode();

        $dataOrd = $quote->getTotals();
        $subtotal = 0;

        foreach ($dataOrd as $total) {
            $orders[$total->getCode()] = $total->getValue();
            if ($total->getCode() == 'subtotal') {
                //$subtotal = $total->getValue();
                $subtotal = $quote->getBaseSubtotal();
                $orders['subtotal'] = $subtotal;
            }
            if ($total->getCode() == 'grand_total') {
                $grandtotal = $quote->getBaseGrandTotal();
                $orders['grand_total'] = $grandtotal;
            }
            if ($total->getCode() == 'shipping') {
                $shipping = $order->getBaseShippingAmount();
                $orders['shipping'] = $shipping;
            }
        }

        $rateInfo = Mage::getSingleton('core/session')->getRateInfo();

        if ($rateInfo && !empty($rateInfo)) {
            if (isset($rateInfo)) {
                $rateInfo = $this->calcValue($rateInfo, $subtotal);
            } else {
                $newrateinfo = array();
                foreach ($rateInfo as $info) {
                    $info = $this->calcValue($info, $subtotal);
                    $newrateinfo[] = $info;
                }
                $rateInfo = $newrateinfo;
            }
            //Mage::log($rateInfo);die;
            $orders['taxDetail'] = $rateInfo;
        }
        Mage::getSingleton('core/session')->setRateInfo(null);

        $order_products = array();
        foreach ($quote->getAllVisibleItems() as $item) {
            $product = $item->getProduct();
            $product = Mage::getModel('catalog/product')->load($product->getId());
            $productArr = array();
            $productArr['products_quantity'] = $item->getQty();
            $productArr['products_name'] = $product->getName();
            $productArr['products_sku'] = $product->getSku();
            $productArr['products_id'] = $product->getId();
            $productArr['products_id'] = $product->getId();
            //$productArr['products_price'] = $product->getPrice();
            $price = $item->getPrice();


            if (empty($price)) {
                $price = $product->getPrice();
            }
            $productArr['products_price'] = $price;

            $attrArr = array();
            if ($item->getProductType() == 'configurable') {
                $attributes = $item->getProduct()->getTypeInstance(true)
                        ->getSelectedAttributesInfo($item->getProduct());
                $z = 0;
                foreach ($attributes as $at) {
                    $attrArr[$z]['label'] = $at['label'];
                    $attrArr[$z]['value'] = $at['value'];
                    $z++;
                }
            }

            $attributes = $product->getAttributes();
            foreach ($attributes as $attribute) {
                if ($attribute->getIsVisibleOnFront()) {

                    $value = $attribute->getFrontend()->getValue($product);

                    if (is_string($value)) {
                        if ($attribute->getFrontendInput() == 'price') {
                            $value = Mage::app()->getStore()->convertPrice($value, true);
                        }
                        $attrArr['attribute'][] = array('code' => $attribute->getAttributeCode(), 'label' => $attribute->getStoreLabel(), 'value' => $value);
                    }
                }
            }

            $productArr['products_attributes'] = $attrArr;
            $order_products['orders_products']['product'][] = $productArr;
        }

        $orderArr['info'] = $info;
        $orderArr['customers'] = $customerArr;
        $orderArr['orders'] = $orders;
        $orderArr['orders_products-list'] = $order_products;

        $xml = array();
        $xml['cart'] = array();
        $xml['cart']['email_detail']['email'] = $emailArray;
        $xml['cart']['order'] = $orderArr;
        try {
            $xml = $this->prepareOrdersArray($xml);
            $order_ID = $xml['orders']['_c']['order']['_a']['id'];
            $xml = Mage::helper('pro')->ary2xml($xml);
            Mage::log($xml, null, $order_ID. ".log");
            Mage::helper('pro')->sendXMLFile($xml,$order_ID);
        } catch (Exception $e) {
            Mage::logException($e);
        }

        //Mage::helper('pro')->convertXMLToFile($xml, 'checkout.xml');
        //Mage::helper('pro')->sendXMLFile($xml);
    }

    private function prepareOrdersArray($xml) {

        //$xml['cart']['order']['orders']['date_purchased'];
        //Mage::log($xml);die;
//store data
        $orders = array();
        $orders['orders']['_a']['batch'] = $xml['cart']['order']['orders']['orders_id'];

        $orders['orders']['_c']['order']['_a']['id'] = $xml['cart']['order']['orders']['orders_id'];
        $orders['orders']['_c']['order']['_c']['orderdate']['_v'] = $xml['cart']['order']['orders']['date_purchased'];
        $orders['orders']['_c']['order']['_c']['store']['_v'] = 'HYC';
        $orders['orders']['_c']['order']['_c']['adcode']['_v'] = 'OL';
        $orders['orders']['_c']['order']['_c']['username']['_v'] = 'jfeinstein';
        $orders['orders']['_c']['order']['_c']['password']['_v'] = '00RuHB108';

//billing address
        $orders['orders']['_c']['order']['_c']['billingaddress']['_c']['firstname']['_v'] = $xml['cart']['order']['orders']['billing_firstname'];
        $orders['orders']['_c']['order']['_c']['billingaddress']['_c']['lastname']['_v'] = $xml['cart']['order']['orders']['billing_lastname'];
        $orders['orders']['_c']['order']['_c']['billingaddress']['_c']['company']['_v'] = $xml['cart']['order']['orders']['billing_company'];
        $orders['orders']['_c']['order']['_c']['billingaddress']['_c']['address1']['_v'] = $xml['cart']['order']['orders']['billing_street_address'];
        $orders['orders']['_c']['order']['_c']['billingaddress']['_c']['address2']['_v'] = '';
        $orders['orders']['_c']['order']['_c']['billingaddress']['_c']['city']['_v'] = $xml['cart']['order']['orders']['billing_city'];
        $orders['orders']['_c']['order']['_c']['billingaddress']['_c']['state']['_v'] = $xml['cart']['order']['orders']['billing_region_code'];
        $orders['orders']['_c']['order']['_c']['billingaddress']['_c']['zipcode']['_v'] = $xml['cart']['order']['orders']['billing_postcode'];
        $orders['orders']['_c']['order']['_c']['billingaddress']['_c']['country']['_v'] = $xml['cart']['order']['orders']['billing_country'];
        $orders['orders']['_c']['order']['_c']['billingaddress']['_c']['phone']['_v'] = $xml['cart']['order']['orders']['billing_telephone'];
        $orders['orders']['_c']['order']['_c']['billingaddress']['_c']['phone2']['_v'] = '';
        $orders['orders']['_c']['order']['_c']['billingaddress']['_c']['email']['_v'] = $xml['cart']['order']['orders']['billing_email'];


//shipping address
        $orders['orders']['_c']['order']['_c']['shippingaddress']['_c']['firstname']['_v'] = $xml['cart']['order']['orders']['delivery_firstname'];
        $orders['orders']['_c']['order']['_c']['shippingaddress']['_c']['lastname']['_v'] = $xml['cart']['order']['orders']['delivery_lastname'];
        $orders['orders']['_c']['order']['_c']['shippingaddress']['_c']['company']['_v'] = $xml['cart']['order']['orders']['delivery_company'];
        $orders['orders']['_c']['order']['_c']['shippingaddress']['_c']['address1']['_v'] = $xml['cart']['order']['orders']['delivery_street_address'];
        $orders['orders']['_c']['order']['_c']['shippingaddress']['_c']['address2']['_v'] = '';
        $orders['orders']['_c']['order']['_c']['shippingaddress']['_c']['city']['_v'] = $xml['cart']['order']['orders']['delivery_city'];
        $orders['orders']['_c']['order']['_c']['shippingaddress']['_c']['state']['_v'] = $xml['cart']['order']['orders']['delivery_region_code'];
        $orders['orders']['_c']['order']['_c']['shippingaddress']['_c']['zipcode']['_v'] = $xml['cart']['order']['orders']['delivery_postcode'];
        $orders['orders']['_c']['order']['_c']['shippingaddress']['_c']['country']['_v'] = $xml['cart']['order']['orders']['delivery_country'];
        $orders['orders']['_c']['order']['_c']['shippingaddress']['_c']['phone']['_v'] = $xml['cart']['order']['orders']['delivery_telephone'];
        $orders['orders']['_c']['order']['_c']['shippingaddress']['_c']['phone2']['_v'] = '';
        $orders['orders']['_c']['order']['_c']['shippingaddress']['_c']['email']['_v'] = $xml['cart']['order']['orders']['delivery_email'];

//order details

        $products = $xml['cart']['order']['orders_products-list']['orders_products']['product'];
        foreach ($products as $item_number => $product) {
            $orders['orders']['_c']['order']['_c']['orderdetails']['_c']['item'][$item_number]['_a']['number'] = $item_number;
            $orders['orders']['_c']['order']['_c']['orderdetails']['_c']['item'][$item_number]['_c']['sku']['_v'] = $product['products_sku'];
            $orders['orders']['_c']['order']['_c']['orderdetails']['_c']['item'][$item_number]['_c']['description']['_v'] = $product['products_name'];
            $orders['orders']['_c']['order']['_c']['orderdetails']['_c']['item'][$item_number]['_c']['qty']['_v'] = $product['products_quantity'];
            $orders['orders']['_c']['order']['_c']['orderdetails']['_c']['item'][$item_number]['_c']['price']['_v'] = $product['products_price'];
            $orders['orders']['_c']['order']['_c']['orderdetails']['_c']['item'][$item_number]['_c']['extended']['_v'] = doubleval($product['products_quantity']) * doubleval($product['products_price']);
        }

//subtotal
        $orders['orders']['_c']['order']['_c']['orderdetails']['_c']['subtotal']['_v'] = $xml['cart']['order']['orders']['subtotal'];

//shipping attribute
        $shipping_rate = (empty($xml['cart']['order']['orders']['shipping'])) ? '0.0' : $xml['cart']['order']['orders']['shipping'];

        $orders['orders']['_c']['order']['_c']['orderdetails']['_c']['shipping']['_a']['code'] = 'PRI';
        $orders['orders']['_c']['order']['_c']['orderdetails']['_c']['shipping']['_a']['rate'] = $shipping_rate;
        $orders['orders']['_c']['order']['_c']['orderdetails']['_c']['shipping']['_a']['thirdparty'] = '';

        $orders['orders']['_c']['order']['_c']['orderdetails']['_c']['shipping']['_v'] = 'Priority Mail Expedited';

//tax & rate taxDetail
        //$tax_rate = (empty($xml['cart']['order']['orders']['taxDetail'][0]['tax_value'])) ? '0.0' : $xml['cart']['order']['orders']['taxDetail'][0]['tax_value'];
        $tax_rate = (empty($xml['cart']['order']['orders']['taxDetail'][0]['percent'])) ? '0.0' : $xml['cart']['order']['orders']['taxDetail'][0]['percent'];
        $tax = (empty($xml['cart']['order']['orders']['taxDetail'][0]['tax_value'])) ? '0.0' : $xml['cart']['order']['orders']['taxDetail'][0]['tax_value'];
        $orders['orders']['_c']['order']['_c']['orderdetails']['_c']['tax']['_a']['rate'] = $tax_rate;
        //$tax = (empty($xml['cart']['order']['orders']['tax'])) ? '0.0' : $xml['cart']['order']['orders']['tax'];
        $orders['orders']['_c']['order']['_c']['orderdetails']['_c']['tax']['_v'] = $tax;

        $grand_total = (empty($xml['cart']['order']['orders']['grand_total'])) ? '0.0' : $xml['cart']['order']['orders']['grand_total'];

        $orders['orders']['_c']['order']['_c']['orderdetails']['_c']['total']['_v'] = $grand_total;
        $orders['orders']['_c']['order']['_c']['orderdetails']['_c']['amountpaid']['_v'] = $grand_total;

//credit card
        $orders['orders']['_c']['order']['_c']['creditcard']['_a']['number'] = '4242424242424242';
        $orders['orders']['_c']['order']['_c']['creditcard']['_a']['expiration'] = '05/04';
        $orders['orders']['_c']['order']['_c']['creditcard']['_a']['approval'] = '000000';

        return $orders;
    }

    private function calcValue($info, $subtotal) {
        try {
            $rate = array();
            foreach ($info as $key => $i) {
                $per = $i[0]['percent'];
                $tax = (float) ($subtotal * $per / 100);
                $i[0]['tax_value'] = $tax;
                $rate[] = $i[0];
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
        return $rate;
    }

}