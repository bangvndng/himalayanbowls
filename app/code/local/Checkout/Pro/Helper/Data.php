<?php

/**
 * MAGENTO - RUSH ORDERS POST SYSTEM
 * GREEN GLOBAL Co.,ltd
 *
 * @author bangvn
 * @link bangvn@toancauxanh.vn
 *
 * @uses this file is used when a cron run.
 */
class Checkout_Pro_Helper_Data extends Mage_Core_Helper_Abstract {

        private function xmlClean($strin) {
                $strout = null;

                for ($i = 0; $i < strlen($strin); $i++) {
                        $ord = ord($strin[$i]);

                        if (($ord > 0 && $ord < 32) || ($ord >= 127)) {
                                $strout .= "&amp;#{$ord};";
                        } else {
                                switch ($strin[$i]) {
                                        case '<':
                                                $strout .= '&lt;';
                                                break;
                                        case '>':
                                                $strout .= '&gt;';
                                                break;
                                        case '&':
                                                $strout .= '&amp;';
                                                break;
                                        case '"':
                                                $strout .= '&quot;';
                                                break;
                                        default:
                                                $strout .= $strin[$i];
                                }
                        }
                }

                return $strout;
        }

        public function convertToXML($arr, $parent = "", $depth = 1) {
                $xml = "";
                if (is_array($arr)) {
                        foreach ($arr as $key => $val) {
                                if (is_int($key)) {
                                        $key = $parent;
                                }
                                $xml .= "\n";
                                for ($i = 1; $i < $depth; $i++) {
                                        $xml .= "\t";
                                }
                                $key = str_replace(" ", '_', $key);
                                $xml .= "<$key>";
                                if ($key == 'cart') {
                                        $xml .= $this->getParameterFile();
                                }
                                if (!is_array($val)) {
                                        $xml .= utf8_encode($this->xmlClean($val));
                                } else if (!empty($val)) {
                                        $xml .= $this->convertToXML($val, $key, $depth + 1);
                                }
                                if (is_array($val)) {
                                        $xml .= "\n";
                                }
                                if (is_array($val)) {
                                        for ($i = 1; $i < $depth; $i++) {
                                                $xml .= "\t";
                                        }
                                }
                                $xml .= "</$key>";
                        }
                }
                return $xml;
        }

        public function getParameterFile() {
                $file = Mage::getBaseDir() . DS . 'media' . DS . 'xml' . DS . 'parameters.xml';
                $content = file_get_contents($file);
                return $content;
        }

        public function convertXMLToFile($arr, $name) {
                $xml = $this->convertToXML($arr);
                $xml = '<?xml version="1.0" encoding="ISO-8859-1"?>' . $xml;
                $path = Mage::getBaseDir() . DS . $name;
                $fh = fopen($path, 'w');
                fwrite($fh, $xml);
                fclose($fh);
        }

        /*
         * This post XML file to Rush Order
         *
         * @var @arr
         * @var @order_ID
         *
         * @using #order_ID-result.log
         * @using #order_ID-response.log
         * @using #order_ID-post-prepare.log
         *
         * @using ro-posting.log
         *
         *
         */

        public function sendXMLFile($arr, $order_ID) {
                /**
                  $xml = $this->convertToXML($arr);
                  $xml = '<?xml version="1.0" encoding="UTF-8"?>' . $xml;
                 */
                /**
                  $path = Mage::getBaseDir() . DS . 'checkout.xml';
                  $xml = file_get_contents($path);
                 */
                $xml = $arr;
                $xml = '<?xml version="1.0" encoding="ISO-8859-1"?>' . PHP_EOL . $xml;

                //$target_url = "https://orders.rushorder.com/xml3/parser.cfm";
                $url = Mage::getStoreConfig('pro/checkout/url');
                if (!empty($url)) {
                        $target_url = $url;
                }
				$target_url = "https://api.rushorder.com/orders/index.cfm";


                $response_message = "";
                $response_body = "";
                $status_code = 'posting';
                $retrying_count = 0;

                $msg = "RUSHORDER Posting service started. Order ID #" . $order_ID;
                Mage::log($msg, null, "ro-posting.log");

                while ($status_code == 'posting' || $status_code == 'retry') {

                        // Prepare the xml file
                        if ($status_code == 'retry') {
                                if ($retrying_count == 3) {

                                        // Quit after retrying 3 times
                                        $response_message = "Rush Order Posting Process Failed - Order ID #" . $order_ID;
                                        $response_body = 'Order #' . $order_ID . ' post failed due to no response from Rush Order after 3 retries, please try again later';
                                        Mage::log($response_message);
                                        Mage::log($response_body);

                                        Mage::log($order_ID, null, "ro-posting-failed" . ".log");

                                        $msg = "Failed to post after 3 retrying. Order ID #" . $order_ID . ". The file was moved to repost queue.";
                                        Mage::log($msg, null, "ro-posting.log");

                                        $status_code = 'failed';
                                        $this->sendEmail($response_message, $response_body, 'failed');

                                        file_put_contents('post' . '/' . $order_ID . '.xml', $xml);
                                        file_put_contents('postinfo' . '/' . $order_ID . '.info', 1);

                                        break;
                                } else {

                                        $msg = 'Retrying posting Order to ' . $target_url . ' : ' . $retrying_count . 'times. ' . "Order ID #" . $order_ID;
                                        Mage::log($msg, null, "ro-posting.log");
                                }
                        } else {
                                Mage::log($xml, null, $order_ID . "-post-prepare" . ".log");

                                $msg = 'Posting Order to ' . $target_url . "Order ID #" . $order_ID;
                                Mage::log($msg, null, "ro-posting.log");
                        }
                        // Post the xml
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $target_url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        $result = curl_exec($ch);
                        curl_close($ch);

                        // Loging the responsed
                        Mage::log($result, null, $order_ID . "-result" . ".log");

                        if (empty($result) || !isset($result)) {

                                //Retry posting
                                $msg = 'Empty result due to no response or connection problem, retrying posting. ' . "Order ID #" . $order_ID;
                                Mage::log($msg, null, "ro-posting.log");

                                $response_message = "";
                                $status_code = 'retry';
                                $retrying_count++;
                                continue;
                        }else{
                                $msg = 'Result ok. ' . "Order ID #" . $order_ID;
                                Mage::log($msg, null, "ro-posting.log");
                                Mage::log($result, null, "ro-posting-detail.log");
                        }

                        $response = (array) simplexml_load_string($result, 'SimpleXMLElement', LIBXML_NOCDATA);

                        Mage::log($response, null, $order_ID . "-response" . ".log");

                        if (empty($response['order']) || !isset($response['order'])) {

                                //Retry posting
                                $msg = 'Can not analyse response, retrying posting. ' . "Order ID #" . $order_ID;
                                Mage::log($msg, null, "ro-posting.log");

                                $response_message = "";
                                $status_code = 'retry';
                                $retrying_count++;
                                continue;
                        }else{

                                $msg = 'Response analysed ok. ' . "Order ID #" . $order_ID;
                                Mage::log($msg, null, "ro-posting.log");
                                Mage::log($response['order'], null, "ro-posting-detail.log");
                        }

                        $attrs = $response['order']->attributes();

                        Mage::log($attrs, null, "ro-posting.log");

                        foreach ($attrs as $key => $attr) {
                                $attr_val = (array) $attr[0];
                                if ($key == 'status') {
                                        $status_code = $attr_val[0];
                                }
                        }

                        Mage::log($status_code, null, "ro-posting.log");

                        if ($status_code == 'Success') {
                                //Stop posting , email administrator
                                $response_message = "Rush Order Posting Success - Order ID #" . $order_ID;
                                $response_body = 'Order #' . $order_ID . ' posted';
                                Mage::log($response_message, null, "ro-posting-detail.log");
                                Mage::log($response_body, null, "ro-posting-detail.log");

                                $this->sendEmail($response_message, $response_body);
                                break;
                        } else {
                                if ($status_code == 'Duplicate') {
                                        //Stop posting, email administrator
                                        $response_message = "Duplicated Post, Skipping ...!";
                                        $response_body = 'Order #' . $order_ID . ' Duplicated <br>';
                                        $response_description = $response;

                                        Mage::log($response_message, null, "ro-posting-detail.log");
                                        Mage::log($response_body, null, "ro-posting-detail.log");
                                        Mage::log($response_description, null, "ro-posting-detail.log");

                                        $this->sendEmail($response_message, $response_body);
                                        break;
                                } else {
                                        if ($status_code == 'Failed') {
                                                //Stop posting, email administrator
                                                $response_message = "Rush Order Posting Process Failed! - Order ID #" . $order_ID;
                                                $response_body = 'Order #' . $order_ID . ' was failed';
                                                $response_error = '';
                                                $response_description = '';
                                                $order_childs = $response['order']->children();
                                                foreach ($order_childs as $key => $order_child) {
                                                        $childs = (array) $order_child[0];
                                                        if ($key == 'error') {
                                                                $response_error = $childs[0];
                                                        } else {
                                                                if ($key == 'text') {
                                                                        $response_description = $childs[0];
                                                                }
                                                        }
                                                }
                                                Mage::log($response_message, null, "ro-posting-detail.log");
                                                Mage::log($response_body, null, "ro-posting-detail.log");
                                                Mage::log('Error code : ' . $response_error, null, "ro-posting-detail.log");
                                                Mage::log('Error descriptions : ' . $response_description, null, "ro-posting-detail.log");

                                                Mage::log($order_ID, null, "ro-posting-failed" . ".log");

                                                $response_body .= '<br>' . 'Error code : ' . $response_error . '<br>' . 'Error Description : ' . $response_description . '<br>';
                                                $this->sendEmail($response_message, $response_body, 'failed');
                                                break;
                                        } else {
                                                //Retry posting
                                                $response_message = "";
                                                $status_code = 'retry';
                                                $retrying_count++;
                                        }
                                }
                        }
                }

                $msg = "RUSHORDER Posting service ended. Order ID #" . $order_ID . ". Overal status :" . $status_code . ". Connection is now close.";
                Mage::log($msg, null, "ro-posting.log");
        }

        public function sendEmail($subject, $body, $mail_code = 'Success') {
                $fromEmail = "support@himalayanbowls.com"; // sender email address
                $fromName = "Himalayanbowls Administrator"; // sender name

                $toEmail1 = "joseph@himalayanbowls.com"; // recipient email address
                $toName1 = "Joseph"; // recipient name

                $toEmail2 = "bangvn@webdev.vn"; // recipient email address
                $toName2 = "Bang Vu Ngoc"; // recipient name

                $mail = new Zend_Mail();
                $mail->setBodyText($body);
                $mail->setFrom($fromEmail, $fromName);

                if ($mail_code == 'Success') {
                        $mail->addTo($toEmail2, $toName2);
                } else {
                        //$mail->addTo($toEmail1, $toName1);
                        $mail->addTo($toEmail2, $toName2);
                }

                $mail->setSubject($subject);
                try {
                        $mail->send();
                } catch (Exception $ex) {
                        // I assume you have your custom module.
                        // If not, you may keep 'customer' instead of 'yourmodule'.
                        Mage::getSingleton('core/session')->addError(Mage::helper('pro')->__('Unable to send email.'));
                        Mage::log($ex);
                }
        }

        function xml2ary(&$string) {
                $parser = xml_parser_create();
                xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
                xml_parse_into_struct($parser, $string, $vals, $index);
                xml_parser_free($parser);

                $mnary = array();
                $ary = &$mnary;
                foreach ($vals as $r) {
                        $t = $r['tag'];
                        if ($r['type'] == 'open') {
                                if (isset($ary[$t])) {
                                        if (isset($ary[$t][0]))
                                                $ary[$t][] = array(); else
                                                $ary[$t] = array($ary[$t], array());
                                        $cv = &$ary[$t][count($ary[$t]) - 1];
                                } else
                                        $cv = &$ary[$t];
                                if (isset($r['attributes'])) {
                                        foreach ($r['attributes'] as $k => $v)
                                                $cv['_a'][$k] = $v;
                                }
                                $cv['_c'] = array();
                                $cv['_c']['_p'] = &$ary;
                                $ary = &$cv['_c'];
                        } elseif ($r['type'] == 'complete') {
                                if (isset($ary[$t])) { // same as open
                                        if (isset($ary[$t][0]))
                                                $ary[$t][] = array(); else
                                                $ary[$t] = array($ary[$t], array());
                                        $cv = &$ary[$t][count($ary[$t]) - 1];
                                } else
                                        $cv = &$ary[$t];
                                if (isset($r['attributes'])) {
                                        foreach ($r['attributes'] as $k => $v)
                                                $cv['_a'][$k] = $v;
                                }
                                $cv['_v'] = (isset($r['value']) ? $r['value'] : '');
                        } elseif ($r['type'] == 'close') {
                                $ary = &$ary['_p'];
                        }
                }

                $this->_del_p($mnary);
                return $mnary;
        }

// _Internal: Remove recursion in result array
        function _del_p(&$ary) {
                foreach ($ary as $k => $v) {
                        if ($k === '_p')
                                unset($ary[$k]);
                        elseif (is_array($ary[$k]))
                                $this->_del_p($ary[$k]);
                }
        }

// Array to XML
        function ary2xml($cary, $d=0, $forcetag='') {
                $res = array();
                foreach ($cary as $tag => $r) {
                        if (isset($r[0])) {
                                $res[] = $this->ary2xml($r, $d, $tag);
                        } else {
                                if ($forcetag)
                                        $tag = $forcetag;
                                $sp = str_repeat("\t", $d);
                                $res[] = "$sp<$tag";
                                if (isset($r['_a'])) {
                                        foreach ($r['_a'] as $at => $av)
                                                $res[] = " $at=\"$av\"";
                                }
                                $res[] = ">" . ((isset($r['_c'])) ? "\n" : '');
                                if (isset($r['_c']))
                                        $res[] = $this->ary2xml($r['_c'], $d + 1);
                                elseif (isset($r['_v']))
                                        $res[] = $r['_v'];
                                $res[] = (isset($r['_c']) ? $sp : '') . "</$tag>\n";
                        }
                }
                return implode('', $res);
        }

// Insert element into array
        function ins2ary(&$ary, $element, $pos) {
                $ar1 = array_slice($ary, 0, $pos);
                $ar1[] = $element;
                $ary = array_merge($ar1, array_slice($ary, $pos));
        }

}