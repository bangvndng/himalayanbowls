<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Product list
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Catalog_Block_Product_List extends Mage_Catalog_Block_Product_Abstract {

    /**
     * Default toolbar block name
     *
     * @var string
     */
    protected $_defaultToolbarBlock = 'catalog/product_list_toolbar';

    /**
     * Product Collection
     *
     * @var Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected $_productCollection;
    protected $_currentlyInCart;
    protected $_currentCustomerSession;
    protected $_currentQuote;
    protected $_currentCheckoutSession;

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    protected function _getProductCollection() {
        if (is_null($this->_productCollection)) {
            $this->_currentlyInCart = null;
            $layer = $this->getLayer();
            /* @var $layer Mage_Catalog_Model_Layer */
            if ($this->getShowRootCategory()) {
                $this->setCategoryId(Mage::app()->getStore()->getRootCategoryId());
            }
            // if this is a product view page
            if (Mage::registry('product')) {
                // get collection of categories this product is associated with
                $categories = Mage::registry('product')->getCategoryCollection()
                        ->setPage(1, 1)
                        ->load();
                // if the product is associated with any category
                if ($categories->count()) {
                    // show products from this category
                    $this->setCategoryId(current($categories->getIterator()));
                }
            }
            $origCategory = null;
            if ($this->getCategoryId()) {
                $category = Mage::getModel('catalog/category')->load($this->getCategoryId());
                if ($category->getId()) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                }
            }
            // dunghd add code 17/02/2011
            // $this->_productCollection = $layer->getProductCollection();
            // dunghd add for fix group by 'sku_position'
            // order by 'sku_position +0 desc' for fix order value varchar
            if ($this->getRequest()->getQuery('order') != '')
                $this->_productCollection = $layer->getProductCollection()->groupByAttributeFix('sku_position');
            else
                $this->_productCollection = $layer->getProductCollection()->groupByAttributeFix('sku_position')->addAttributeToSort('sku_position', '+0 asc');
            // dunghd fix for remove from product after add to cart
            // 18/12/2011
            // Backup current session
            $cur_session = session_encode();
            //print_r($cur_session);
            // Fetch All online session      
            $_resource = Mage::getSingleton('core/resource');
            $_conn = $_resource->getConnection('core_read');
            $_str = 'select * from core_session where session_expires > ' . time();
            $_query = $_conn->query($_str);
            $_results = $_query->fetchAll();
            // Find all online shopping cart
            $_quotes = array();
            foreach ($_results as $key => $visitor) {
                session_decode($visitor['session_data']);
                $_quotes[] = @$_SESSION['core']['visitor_data']['quote_id'];
            }
            $_quotes = array_unique($_quotes);
//            print_r('session quote : ');
//            print_r($_quotes);
//            print_r('<br>');
            // restore current session
            session_decode($cur_session);
            //$visitor = Mage::getSingleton('core/session',array('name' => 'frontend'));
            // find all product type 1
            $_products = array();
            foreach ($_quotes as $item) {
                $quote = Mage::getModel('sales/quote');
                $quote->load($item);
                foreach ($quote->getAllItems() as $prod) {
                    $qty = (int) $prod->getProduct()->getStockItem()->getQty();
                    if ($qty === 1) {
                        $_products[] = $prod->getProduct()->entity_id;
                    }
                };
            }
			
			/**
			Temporary comment
//            print_r('all product type 1 that currently in all cart: ');
//            print_r($_products);
//            print_r('<br>');
            
            // Get current products in cart in current checkout session 
            $cart = Mage::getSingleton('checkout/cart');
            $items = $cart->getQuote()->getAllVisibleItems();
            $items_currently_in_cart = array();
            $items_still_displaying = array();
            foreach ($items as $item) {
                $items_currently_in_cart[] = $item->getProduct()->getId();
                if (in_array($item->getProduct()->getId(), $_products)) {
                    foreach ($_products as $no => $_product) {
                        if ($_products[$no] == $item->getProduct()->getId()) {
                            unset($_products[$no]);
                            $items_still_displaying[] = $item->getProduct()->getId();
                        }
                    }
                }
            }
            
            $_products = array_values($_products);
//            print_r('items currently in my cart: ');
//            print_r($items_currently_in_cart);
//            print_r('<br>');

//            print_r('products will be remove from your view : ');
//            print_r($_products);
//            print_r('<br>');

//            print_r('items still displaying on your view: ');
//            print_r($items_still_displaying);
//            print_r('<br>');
            $this->_currentlyInCart = $items_still_displaying;
			
			*/
			
            //Zend_Debug::dump($_products);
            if (count($_products))
                $this->_productCollection->addAttributeToFilter('entity_id', array('nin' => $_products));
            //echo $this->_productCollection->getSelectSql();
            //exit;
            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }
            
            // dunghd add code 21/02/2011        
            // fix size
            $sql = $this->_productCollection->getSelectCountSql();
            $totalRecordsFix = count($this->_productCollection->getConnection()->fetchAll($sql, $this->_productCollection->getBindParams()));
            $this->_productCollection->setSize($totalRecordsFix);
        }
        return $this->_productCollection;
    }

    /**
     * Get catalog layer model
     *
     * @return Mage_Catalog_Model_Layer
     */
    public function getLayer() {
        $layer = Mage::registry('current_layer');
        if ($layer) {
            return $layer;
        }
        return Mage::getSingleton('catalog/layer');
    }

    /**
     * Retrieve loaded category collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getLoadedProductCollection() {
        return $this->_getProductCollection();
    }

    /**
     * Retrieve loaded category collection
     *
     * @return array
     */
    public function getCurrentlyInCartProductsIds() {
        return $this->_currentlyInCart;
    }

    /**
     * Retrieve current view mode
     *
     * @return string
     */
    public function getMode() {
        return $this->getChild('toolbar')->getCurrentMode();
    }

    /**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     */
    protected function _beforeToHtml() {
        /* $toolbar = $this->getLayout()->createBlock('catalog/product_list_toolbar', microtime());
          if ($toolbarTemplate = $this->getToolbarTemplate()) {
          $toolbar->setTemplate($toolbarTemplate);
          } */
        $toolbar = $this->getToolbarBlock();

        // called prepare sortable parameters
        $collection = $this->_getProductCollection();

        // use sortable parameters
        if ($orders = $this->getAvailableOrders()) {
            $toolbar->setAvailableOrders($orders);
        }
        if ($sort = $this->getSortBy()) {
            $toolbar->setDefaultOrder($sort);
        }
        if ($dir = $this->getDefaultDirection()) {
            $toolbar->setDefaultDirection($dir);
        }
        if ($modes = $this->getModes()) {
            $toolbar->setModes($modes);
        }

        // set collection to toolbar and apply sort
        $toolbar->setCollection($collection);

        $this->setChild('toolbar', $toolbar);
        Mage::dispatchEvent('catalog_block_product_list_collection', array(
            'collection' => $this->_getProductCollection()
        ));

        $this->_getProductCollection()->load();
        Mage::getModel('review/review')->appendSummary($this->_getProductCollection());
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve Toolbar block
     *
     * @return Mage_Catalog_Block_Product_List_Toolbar
     */
    public function getToolbarBlock() {
        if ($blockName = $this->getToolbarBlockName()) {
            if ($block = $this->getLayout()->getBlock($blockName)) {
                return $block;
            }
        }
        $block = $this->getLayout()->createBlock($this->_defaultToolbarBlock, microtime());
        return $block;
    }

    /**
     * Retrieve additional blocks html
     *
     * @return string
     */
    public function getAdditionalHtml() {
        return $this->getChildHtml('additional');
    }

    /**
     * Retrieve list toolbar HTML
     *
     * @return string
     */
    public function getToolbarHtml() {
        return $this->getChildHtml('toolbar');
    }

    public function setCollection($collection) {
        $this->_productCollection = $collection;
        return $this;
    }

    public function addAttribute($code) {
        $this->_getProductCollection()->addAttributeToSelect($code);
        return $this;
    }

    public function getPriceBlockTemplate() {
        return $this->_getData('price_block_template');
    }

    /**
     * Retrieve Catalog Config object
     *
     * @return Mage_Catalog_Model_Config
     */
    protected function _getConfig() {
        return Mage::getSingleton('catalog/config');
    }

    /**
     * Prepare Sort By fields from Category Data
     *
     * @param Mage_Catalog_Model_Category $category
     * @return Mage_Catalog_Block_Product_List
     */
    public function prepareSortableFieldsByCategory($category) {
        if (!$this->getAvailableOrders()) {
            $this->setAvailableOrders($category->getAvailableSortByOptions());
        }
        $availableOrders = $this->getAvailableOrders();
        if (!$this->getSortBy()) {
            if ($categorySortBy = $category->getDefaultSortBy()) {
                if (!$availableOrders) {
                    $availableOrders = $this->_getConfig()->getAttributeUsedForSortByArray();
                }
                if (isset($availableOrders[$categorySortBy])) {
                    $this->setSortBy($categorySortBy);
                }
            }
        }

        return $this;
    }

}