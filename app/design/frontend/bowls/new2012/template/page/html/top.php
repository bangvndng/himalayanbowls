<div class="him-logo"><a href="index.php"><img src="<?= $img_dir ?>/hym-logo.png" width="279" height="67" alt="" /></a></div>
<div class="him-top-menu">
    <ul class="clearfix">
        <li><a href="index.php">Home</a></li>
        <li><a href="login.php">My Account</a></li>
        <li><a href="#">My Singing Bowl List</a></li>
        <li><a href="cart.php">View Cart</a></li>
        <li><a href="customer-service.php"><?php echo $this->__('Customer Service') ?></a></li>
        <li class="last"><a href="contact.php">Contact Us</a></li>
    </ul>
    <div class="clear"></div>
    <div class="him-sect">
        <table cellpadding="0" cellspacing="0">
            <tr>
                <td class="him-reg">
                  <!--<a href="#"><img src="<?= $img_dir ?>/flag-us.jpg" width="24" height="14" alt="" /></a>&nbsp;&nbsp;<a href="#"><img src="<?= $img_dir ?>/flag-german.jpg" width="24" height="14" alt="" /></a>&nbsp;&nbsp;
                  Currency : <select><option>USD</option><option>Euro</option></select>-->
                </td>
                <td>
                    <div class="him-cart">
                        <a href="cart.php">10 items in your cart: <b>$210.00</b></a>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="clear"></div>
<div class="him-announce"><?php echo $this->__("Sounds of Enlightenment - Finest Quality Tibetan Singing Bowls & Tibetan Instruments") ?></div>