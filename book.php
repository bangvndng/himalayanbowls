<?php

/**
 * MAGENTO - BOOK DOWNLOAD
 * GREEN GLOBAL Co.,ltd
 * 
 * @author bangvn
 * @link bangvn@toancauxanh.vn
 * 
 * @uses this file is used when a cron run.
 */
?>
<?php 
$file = $_GET["file"];

$file = 'media/pdf/file/The_Singing_Bowl_Book_Sample.pdf';

if (file_exists($file)) {

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header("Content-Type: application/force-download");
    header('Content-Disposition: attachment; filename=' . urlencode(basename($file)));
    // header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    readfile($file);
    exit;
}

?>